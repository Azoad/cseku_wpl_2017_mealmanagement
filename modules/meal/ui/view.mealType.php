<?php

include_once 'blade/view.mealType.blade.php';
include_once COMMON.'class.common.php';

?>
<div class="container">
    <div class="row">
        <h3 class="text-center" style="color: green"><b>Create Meal Type</b></h3>
        <hr>

        <form method="post" class="form-horizontal col-md-7 col-md-offset-2">
            <div class="form-group">
                <label for="mealTypeName" class="control-label">Meal Type</label>
                <input type="text" name="mealTypeName" id="mealTypeName" class="form-control" value="<?php if (isset($_GET['edit'])) echo $getRow->getMealTypeName(); ?>">
            </div>
            <div class="form-group">
                <label for="mealTypeTime" class="control-label">Time</label>
                <input type="time" name="mealTypeTime" id="mealTypeTime" class="form-control" value="<?php if (isset($_GET['edit'])) echo $getRow->getMealTypeTime(); ?>">
            </div>

            <div class="form-group">
                <?php
                if (!isset($_GET['edit'])){
                    ?>
                    <input type="submit" name="btnCreate" id="btnCreate" class="btn btn-default" value="Create">
                <?php
                }else{
                    ?>
                    <input type="submit" name="btnUpdate" id="btnUpdate" class="btn btn-default" value="Update">
                <?php
                }
                ?>


            </div>

            <div class="form-group">
            <h3 align="center" style="color: green"><b>MealType List</b></h3>
        </div>
        </form>

        <table class="table table-bordered table-striped">
            <tr style="background: #003399;color: white">
                <th>Meal Type</th>
                <th>Meal Time</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>

            <?php
            $MealTypeList=$_MealTypeBAO->getAllMealType()->getResultObject();
            foreach ($MealTypeList as $mealtype) {
                ?>
                <tr>
                    <td><?php echo $mealtype->getMealTypeName(); ?></td>
                    <td><?php echo $mealtype->getMealTypeTime(); ?></td>
                    <td><a href="?edit=<?php echo $mealtype->getMealTypeId(); ?>" onclick="return confirm('sure to edit!!')">Edit</a></td>
                    <td><a href="?del=<?php echo $mealtype->getMealTypeId(); ?>" onclick="return confirm('sure to delete!!')">Delete</a></td>
                </tr>
            <?php
            }
            ?>
        </table>
    </div>
</div>