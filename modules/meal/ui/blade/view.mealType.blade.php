<?php

include_once UTILITY.'class.util.php';
include_once MODULES_MEAL.'bao/class.mealTypeBAO.php';

$_DB = DBUtil::getInstance();
$_MealTypeBAO=new MealTypeBAO();

if(isset($_POST['btnCreate'])){
    $MealType=new MealType();
    $MealType->setMealTypeId(Util::getGUID());
    $MealType->setMealTypeName($_DB->secureInput($_POST['mealTypeName']));
    $MealType->setMealTypeTime($_DB->secureInput($_POST['mealTypeTime']));

    $_MealTypeBAO->createMealType($MealType);
}
if (isset($_GET['edit'])){
    $MealType=new MealType();
    $MealType->setMealTypeId($_GET['edit']);
    $getRow=$_MealTypeBAO->getMealTypeById($MealType)->getResultObject();
}

if (isset($_POST['btnUpdate'])){
    $MealType=new MealType();
    $MealType->setMealTypeId($_GET['edit']);
    $MealType->setMealTypeName($_DB->secureInput($_POST['mealTypeName']));
    $MealType->setMealTypeTime($_DB->secureInput($_POST['mealTypeTime']));

    $_MealTypeBAO->updateMealType($MealType);

    header("Location:".PageUtil::$MEAL_TYPE);
}

if (isset($_GET['del'])){
    $MealType=new MealType();
    $MealType->setMealTypeId($_GET['del']);

    $_MealTypeBAO->deleteMealType($MealType);

    header("Location:".PageUtil::$MEAL_TYPE);
}
?>