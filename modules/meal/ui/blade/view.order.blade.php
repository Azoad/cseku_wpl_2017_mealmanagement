<?php

include_once UTILITY.'class.util.php';
include_once MODULES_MEAL.'bao/class.orderBAO.php';
include_once MODULES_MEAL.'bao/class.hallBAO.php';
include_once MODULES_MEAL.'bao/class.foodBAO.php';

$_DB = DBUtil::getInstance();
$_OrderBAO=new OrderBAO();
$_HallBAO=new HallBAO();
$_FoodBAO=new FoodBAO();
$Order=new Order();

if(isset($_POST['btnOrder'])){
    $Order=new Order();
    $Order->setOrderID(Util::getGUID());
    $Order->setOrderMenuId($_DB->secureInput($_POST['btnOrder']));
    $Order->setOrderUserId($_SESSION['globalUser']->getID());
    $Order->setOrderHousingId($_GET['hid']);

    $_OrderBAO->createOrder($Order);
}
