<?php

include_once UTILITY.'class.util.php';
include_once MODULES_MEAL.'bao/class.foodBAO.php';
include_once MODULES_MEAL.'bao/class.hallBAO.php';

$_DB = DBUtil::getInstance();
$_FoodBAO=new FoodBAO();
$_HallBAO=new HallBAO();

if(isset($_POST['btnCreate'])){
    $Food=new Food();
    $Food->setFoodId(Util::getGUID());
    $Food->setFoodName($_DB->secureInput($_POST['foodName']));
    $Food->setFoodCost($_DB->secureInput($_POST['foodCost']));
    $Food->setFoodHousingId($_GET['hid']);

    $_FoodBAO->createFood($Food);
}

if (isset($_GET['edit'])){
    $Food=new Food();
    $Food->setFoodId($_GET['edit']);
    $getRow=$_FoodBAO->getFoodById($Food)->getResultObject();
}

if (isset($_POST['btnUpdate'])){
    $Food=new Food();
    $Food->setFoodId($_GET['edit']);
    $Food->setFoodName($_DB->secureInput($_POST['foodName']));
    $Food->setFoodCost($_DB->secureInput($_POST['foodCost']));
    $Food->setFoodHousingId($_GET['hid']);

    $_FoodBAO->updateFood($Food);
    header("Location:".PageUtil::$FOOD.'?hid='.$_GET['hid']);
}

if (isset($_GET['del'])){
    $Food=new Food();
    $Food->setFoodId($_GET['del']);

    $_FoodBAO->deleteFood($Food);
    header("Location:".PageUtil::$FOOD.'?hid='.$_GET['hid']);
}
?>