<?php

include_once UTILITY.'class.util.php';
include_once MODULES_MEAL.'bao/class.orderDetailsBAO.php';
include_once MODULES_MEAL.'bao/class.orderListBAO.php';
include_once MODULES_MEAL.'bao/class.orderBAO.php';
include_once MODULES_MEAL.'bao/class.hallBAO.php';
include_once MODULES_MEAL.'bao/class.foodBAO.php';
include_once MODULES_MEAL.'bao/class.mealTypeBAO.php';

$_DB = DBUtil::getInstance();
$_OrderDetailsBAO=new OrderDetailsBAO();
$_OrderBAO=new OrderBAO();
$_HallBAO=new HallBAO();
$_FoodBAO=new FoodBAO();
$_OrderListBAO=new OrderListBAO();
$Order=new Order();
$_MealTypeBAO=new mealTypeBAO();