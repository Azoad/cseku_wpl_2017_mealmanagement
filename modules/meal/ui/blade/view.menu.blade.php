<?php

include_once UTILITY.'class.util.php';
include_once MODULES_MEAL.'bao/class.menuBAO.php';
include_once MODULES_MEAL.'bao/class.foodBAO.php';
include_once MODULES_MEAL.'bao/class.mealTypeBAO.php';

$_DB = DBUtil::getInstance();
$_MenuBAO=new MenuBAO();
$_MealTypeBAO=new MealTypeBAO();
$_FoodBAO=new FoodBAO();

if(isset($_POST['btnCreate'])){
    $Menu=new Menu();
    $Menu->setMenuId(Util::getGUID());
    $Menu->setMenuMealTypeId($_DB->secureInput($_POST['mealType']));
    $Menu->setMenuHousingId($_GET['hid']);

    $_MenuBAO->createMenu($Menu);

}

if (isset($_GET['del'])){
    $Menu=new Menu();
    $Menu->setMenuId($_GET['del']);

    $_MenuBAO->deleteMenu($Menu);
    header("Location:".PageUtil::$MENU.'?hid='.$_GET['hid']);
}

if (isset($_POST['btnOffer'])){
    $Offer=new Offer();
    $Offer->setOfferId(Util::getGUID());
    $Offer->setHousingId($_DB->secureInput($_GET['hid']));
    $Offer->setOfferMenuId($_DB->secureInput($_POST['menuId']));
    $Offer->setOfferDate($_DB->secureInput($_POST['offerDate']));

    $_MenuBAO->createOffer($Offer);
}
?>