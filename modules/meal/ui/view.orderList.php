<?php

include_once 'blade/view.orderList.blade.php';
include_once COMMON.'class.common.php';

?>

<div class="container">
    <div class="row">
        <br>
        <h3 class="text-center" style="color: green"><b>Order List</b></h3>
        <hr>
        <form id="searchForm" method="post">
        <div class="form-group">
            <label for="mealType" class="control-label col-md-1">Meal Type :</label>
            <div class="col-md-3">
            <select name="mealType" id="mealType" class="form-control" onchange="jsFunc();">
                <option value="" selected="selected">All</option>
                <?php $MealTypeList=$_MealTypeBAO->getAllMealType()->getResultObject();
                foreach ($MealTypeList as $mealType){
                    ?>
                    <option value="<?php echo $mealType->getMealTypeId(); ?>" <?php
                    if (isset($_POST['mealType'])){
                        if ($_POST['mealType']==$mealType->getMealTypeId()){
                            echo 'selected="selected"';
                        }
                    }
                    ?>><?php echo $mealType->getMealTypeName(); ?></option>
                    <?php
                }
                ?>
            </select>
            </div>
        </div>

        <div class="form-group">
            <label for="mealType" class="control-label col-md-1 col-md-offset-4">Date :</label>
            <div class="col-md-3">
                <input type="date" name="orderDate" id="orderDate" class="form-control" onchange="jsFunc();" value="<?php
                if(isset($_POST['orderDate'])){
                    echo $_POST['orderDate'];
                }
                ?>">
            </div>
        </div>
        </form>
        <table class="table table-bordered table-striped">
            <tr style="background: #003399;color: white">
                <th class="text-center">User Name</th>
                <th class="text-center">Meal Type</th>
                <th class="text-center">Foods</th>
                <th class="text-center">Cost</th>
                <th class="text-center">Date</th>
            </tr>
            <?php
            $OrderList=$_OrderListBAO->getOrderByHallId($_GET['hid'])->getResultObject();
            if(empty($OrderList)){
                ?>
                <tr>
                    <td>
                        
                    </td>
                    <td></td>
                    <td class="text-center" align="right" style="color: #cc0000" >
                        <b><?php
                        echo "No Food List Ordered";
                        ?></b>
                        
                    </td>
                    <td>
                        
                    </td>
                    <td>
                        
                    
                        
                    </td>
                </tr>
                <?php
            }
            else {

            foreach ($OrderList as $order){
                ?>
                <tr>
                    <td class="text-center">
                        <?php
                        $UserName=$_OrderListBAO->getUserNameById($order->getOrderUserId())->getResultObject();
                        echo $UserName->getFirstName().' '.$UserName->getLastName(); ?>
                    </td>
                    <?php
                    $Menu=$_OrderBAO->getMenuByMenuId($order->getOrderMenuId())->getResultObject();
                    $MealType=$_OrderBAO->getMealTypeByMealTypeId($Menu->getMenuMealTypeId())->getResultObject();
                    $FoodMenu=$_OrderBAO->getFoodIdByMenuId($Menu->getMenuId())->getResultObject();
                    $foodlist=null;
                    foreach ($FoodMenu as $foodMenu){
                        $Food=new Food();
                        $Food->setFoodId($foodMenu->getFoodMenuFoodId());
                        $FoodName=$_FoodBAO->getFoodById($Food)->getResultObject();

                        $foodlist.=$FoodName->getFoodName().', ';
                    }

                    ?>
                    <td class="text-center"><?php echo $MealType->getMealTypeName(); ?></td>
                    <td class="text-center"><?php echo $foodlist; ?></td>
                    <td class="text-center"><?php echo $Menu->getMenuTotalCost(); ?></td>
                    <td class="text-center"><?php echo date_format(new DateTime($order->getOrderDate()),"d-M-Y"); ?></td>
                </tr>
                <?php
            }
        }
            ?>
        </table>
    </div>
</div>

<script>
    function jsFunc(){
        $('#searchForm').submit();
    }
</script>