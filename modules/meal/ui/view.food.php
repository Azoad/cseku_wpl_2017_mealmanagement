<?php

include_once 'blade/view.food.blade.php';
include_once COMMON.'class.common.php';

?>
<div class="container">
    <div class="row">
        <h3 class="text-center" style="color: green"><b>Create Food</b></h3>
        <hr>

        <form method="post" class="form-horizontal col-md-7 col-md-offset-2" >
            <div class="form-group">
                <label for="foodName" class="control-label">Food Name</label>
                <input type="text" name="foodName" id="foodName" class="form-control" value="<?php if (isset($_GET['edit'])) echo $getRow->getFoodName(); ?>">
            </div>
            <div class="form-group">
                <label for="foodCost" class="control-label">Food Price</label>
                <input type="text" name="foodCost" id="foodCost" class="form-control" value="<?php if (isset($_GET['edit'])) echo $getRow->getFoodCost(); ?>">
            </div>

            <div class="form-group">
                <?php
                if (!isset($_GET['edit'])){
                    ?>
                    <input type="submit" name="btnCreate" id="btnCreate" class="btn btn-default" value="Create">
                <?php
                }else{
                    ?>
                    <input type="submit" name="btnUpdate" id="btnUpdate" class="btn btn-default" value="Update">
                <?php
                }
                ?>


            </div>
            <div class="form-group">
            <h3 align=" center" style="color: green"><b>Food List</b></h3>
        </div>
        </form>

        <table class="table table-bordered table-striped">
            <tr style="background: #003399;color: white">
                <th>Food Name</th>
                <th>Food Price</th>
                <th>Hall</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>

            <?php
            $FoodList=$_FoodBAO->getFoodByHallId($_GET['hid'])->getResultObject();
            foreach ($FoodList as $food) {
                ?>
                <tr>
                    <td><?php echo $food->getFoodName(); ?></td>
                    <td><?php echo $food->getFoodCost(); ?></td>
                    <td><?php echo $_HallBAO->getHallNameById($food->getFoodHousingId())->getResultObject()->getHousingName(); ?></td>
                    <td><a href="?hid=<?php echo $_GET['hid']; ?>&edit=<?php echo $food->getFoodId(); ?>" onclick="return confirm('sure to edit!!')">Edit</a></td>
                    <td><a href="?hid=<?php echo $_GET['hid']; ?>&del=<?php echo $food->getFoodId(); ?>" onclick="return confirm('sure to delete!!')">Delete</a></td>
                </tr>
            <?php
            }
            ?>
        </table>
    </div>
</div>