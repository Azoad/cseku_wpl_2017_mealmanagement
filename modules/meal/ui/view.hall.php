<?php

include_once 'blade/view.hall.blade.php';
include_once COMMON.'class.common.php';

?>

<div class="container">
    <div class="row">
        <h3 class="text-center" style="color: green"><b>Manage Meal According To Hall</b></h3>

        <hr>
        <table class="table table-bordered table-striped">
            <tr style="background: #003399;color: white">
                <th>Hall Name</th>
                <th>Create Food</th>
                <th>Create Menu</th>
                <th>Order List</th>
                <th>Create Meal Type</th>
            </tr>
            <?php
            $HousingList=$_HallBAO->getAllHall()->getResultObject();
            foreach ($HousingList as $housing){
                ?>
                <tr>
                    <td><?php echo $housing->getHousingName(); ?></td>
                    <td><a href="<?php echo PageUtil::$FOOD.'?hid='.$housing->getHousingId(); ?>">Create Food</a></td>
                    <td><a href="<?php echo PageUtil::$MENU.'?hid='.$housing->getHousingId(); ?>">Create Menu</a></td>
                    <td><a href="<?php echo PageUtil::$ORDER_LIST.'?hid='.$housing->getHousingId(); ?>">Order List</a></td>
                    <td><a href="<?php echo PageUtil::$MEAL_TYPE; ?>">Create Meal Type</a></td>
                </tr>
            <?php
            }
            ?>
        </table>
    </div>
</div>