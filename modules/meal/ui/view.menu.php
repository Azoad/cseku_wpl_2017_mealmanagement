<?php

include_once 'blade/view.menu.blade.php';
include_once COMMON.'class.common.php';

?>
<div class="container">
    <div class="row">
        <h3 class="text-center" style="color: green"><b>Create Menu</b></h3>
        <hr>

        <form method="post" class="form-horizontal col-md-7 col-md-offset-2">
            <div class="form-group">
                <label for="mealType" class="control-label">Meal Types</label>
                <select name="mealType" id="mealType" class="form-control">
                    <option value="" selected="selected" disabled="disabled">Select Meal Type</option>
                    <?php
                    $MealTypeList=$_MealTypeBAO->getAllMealType()->getResultObject();
                    foreach ($MealTypeList as $mealtype) {
                        ?>
                        <option value="<?php echo $mealtype->getMealTypeId(); ?>" ><?php echo $mealtype->getMealTypeName(); ?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>

            <div class="form-group">
                <?php
                if (!isset($_GET['edit'])){
                    ?>
                    <input type="submit" name="btnCreate" id="btnCreate" class="btn btn-default" value="Create">
                    <?php
                }else{
                    ?>
                    <input type="submit" name="btnUpdate" id="btnUpdate" class="btn btn-default" value="Update">
                    <?php
                }
                ?>
            </div>

            <div class="form-group">
                <h3 align="center" style="color: green"><b>Menu List</b></h3>
            </div>

            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content" style="background: #808080;width: 60%" >
                        <div class="modal-header">
                          <!--  <button type="button" class="close" data-dismiss="modal" align="right">&times;</button>-->
                            <h4 class="modal-title" align="center">Offer Menu</h4>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" name="menuId" id="menuId" value="">
                            <input type="date" name="offerDate" id="offerDate" class="form-control" required>
                            <br>
                            <input type="submit" name="btnOffer" id="btnOffer" value="Offer" class="btn btn-success">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal" style="width: 20%">Close</button>
                        </div>
                    </div>
                </div>
            </div>

        </form>

        <table class="table table-bordered table-striped">
            <tr style="background: #003399;color: white">
                <th>Meal Type</th>
                <th>Foods</th>
                <th>Cost</th>
                <th>Delete</th>
                <th>Offer</th>
            </tr>

            <?php
            $MenuList=$_MenuBAO->getMenuByHallId($_GET['hid'])->getResultObject();

            foreach ($MenuList as $menu) {
                $MealType=new MealType();
                $MealType->setMealTypeId($menu->getMenuMealTypeId());
                ?>
                <tr>
                    <td><a href="<?php echo PageUtil::$FOOD_MENU;?>?hid=<?php echo $_GET['hid']; ?>&mid=<?php echo $menu->getMenuId(); ?>"><?php echo $_MealTypeBAO->getMealTypeById($MealType)->getResultObject()->getMealTypeName(); ?></a></td>
                    <td><?php
                    $FoodList=$_MenuBAO->getFoodByMenuId($menu->getMenuId())->getResultObject();
                        $FoodNameList=null;
                        $totalCost=floatval('0');
                    foreach ($FoodList as $food){
                        $FoodName=new Food();
                        $FoodName->setFoodId($food->getFoodId());
                        $totalCost+=floatval($_FoodBAO->getFoodById($FoodName)->getResultObject()->getFoodCost());
                        $FoodNameList.=$_FoodBAO->getFoodById($FoodName)->getResultObject()->getFoodName();
                        $FoodNameList.=','.' ';
                    }
                    if (!empty($FoodNameList)){
                        echo $FoodNameList;
                    }else{
                        echo 'No Food Added';
                    }
                        ?></td>
                    <td><?php
                    $_MenuBAO->addTotalCost($menu->getMenuId(),$totalCost);
                        echo $totalCost; ?></td>
                    <td><a href="?hid=<?php echo $_GET['hid']; ?>&del=<?php echo $menu->getMenuId(); ?>" onclick="return confirm('sure to edit!!')">Delete</a></td>
                    <td><input type="hidden" name="mid" id="mid" value="<?php echo $menu->getMenuId(); ?>">
                        <a data-toggle="modal" data-target="#myModal" href="#" onclick="jsFunction(this);">Offer</a></td>
                </tr>
                <?php
            }
            ?>
        </table>
    </div>
</div>

<script>
    function jsFunction(element) {
        var parent=element.parentNode.parentNode;
        var child=parent.childNodes[9];
        var menu=child.childNodes[0];
        var menuId=menu.value;
        $('#menuId').val(menuId);
    }
</script>