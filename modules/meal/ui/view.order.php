<?php

include_once 'blade/view.order.blade.php';
include_once COMMON.'class.common.php';

?>

<div class="container">
    <div class="row">
        <br>
        <h3 class="text-center" style="color: green"><b>Order As You Wish</b></h3>
        <hr>
        <table class="table table-bordered table-striped">
            <tr style="background: #003399;color: white">
                <th class="text-center">Meal Type</th>
                <th class="text-center">Foods</th>
                <th class="text-center">Cost</th>
                <th class="text-center">Date</th>
                <th class="text-center">Order</th>
            </tr>
            <?php
            $OfferList=$_OrderBAO->getOfferByHallId($_GET['hid'])->getResultObject();
            foreach ($OfferList as $offer){
                ?>
                <tr>
                    <?php
                    $Menu=$_OrderBAO->getMenuByMenuId($offer->getOfferMenuId())->getResultObject();
                    $MealType=$_OrderBAO->getMealTypeByMealTypeId($Menu->getMenuMealTypeId())->getResultObject();
                    $FoodMenu=$_OrderBAO->getFoodIdByMenuId($Menu->getMenuId())->getResultObject();
                    $foodlist=null;
                    foreach ($FoodMenu as $foodMenu){
                        $Food=new Food();
                        $Food->setFoodId($foodMenu->getFoodMenuFoodId());
                        $FoodName=$_FoodBAO->getFoodById($Food)->getResultObject();

                        $foodlist.=$FoodName->getFoodName().', ';
                    }

                    ?>
                    <td class="text-center"><?php echo $MealType->getMealTypeName(); ?></td>
                    <td class="text-center"><?php echo $foodlist; ?></td>
                    <td class="text-center"><?php echo $Menu->getMenuTotalCost(); ?></td>
                    <td class="text-center"><?php echo $offer->getOfferDate(); ?></td>
                    <td class="text-center">
                        <form method="post">
                            <button type="submit" class="btn btn-link" id="btnOrder" name="btnOrder" value="<?php echo $Menu->getMenuId(); ?> "onclick="return confirm('Thanks for ordering...')">Order</button>
                        </form>
                    </td>
                </tr>
                <?php
            }
            ?>
        </table>
    </div>
</div>
