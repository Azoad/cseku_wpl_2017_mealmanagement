<?php

include_once 'blade/view.dashboard.blade.php';
include_once COMMON.'class.common.php';
?>

<div class="container">
    <div class="row">
        <br>
        <h3 class="text-center" style="color: green"><b>Select hall to see offerred Menu</b></h3>
        <hr>
        <table class="table table-bordered table-striped">
            <tr style="background: #003399;color: white">
                <th>Hall</th>
                <th>Offerred Menu List</th>
            </tr>
            <?php
            $HallList=$_HallBAO->getAllHall()->getResultObject();
            foreach ($HallList as $hall){
                ?>
                <tr>
                    <td><?php echo $hall->getHousingName(); ?></td>
                    <td><a href="<?php echo PageUtil::$ORDER.'?hid='.$hall->getHousingId(); ?>">Offerred Menu</a></td>
                </tr>
            <?php
            }
            ?>
        </table>
    </div>
</div>
