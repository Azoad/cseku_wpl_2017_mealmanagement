<?php

include_once COMMON.'class.common.php';
include_once 'blade/view.foodMenu.blade.php';
?>
<script>
    function jsFunc(element){

        var parent=element.parentNode.parentNode;
        var child=parent.childNodes[1];
        var val=child.childNodes[1];
        var foodId=val.value;
        var menuId=$('#mid').val();
        var child2=parent.childNodes[3];
        var val2=child2.childNodes[5];
        var spn=val2.innerText;
        //var spn=parent.childNodes[2].childNodes[2].innerText;
        console.log(spn);
        xhttp=new XMLHttpRequest();
        xhttp.onreadystatechange=function(){
            if(this.readyState==4&&this.status==200){
                element.style.display='none';
                val2.style.display='block';

            }
        };
        xhttp.open("GET","./modules/meal/ui/addFood.php?fid="+foodId+"&mid="+menuId,true);
        xhttp.send();
    }
</script>
<div class="container">
    <div class="container">
            <h3 align="center" style="color: green">Add Food To Menu</h3>
        </div>
    <div class="col-md-12">
        <table class="table table-bordered table-striped">
            <tr style="background: #003399;color: white">
                <th>Food</th>
                <th>Add</th>
            </tr>
            <?php
            $FoodList=$_FoodMenuBAO->getFoodByHallId($_GET['hid'],$_GET['mid'])->getResultObject();
            foreach ($FoodList as $food) {
                ?>
                <tr>
                    <td><?php echo $food->getFoodName(); ?>
                        <input type="hidden" name="fid" id="fid" value="<?php echo $food->getFoodId();?>">
                    </td>
                    <td>
                        <input type="hidden" name="mid" id="mid" value="<?php echo $_GET['mid'];?>">
                        <?php
                        ?>

                        <button type="button" id="btnAdd" class="btn-link" onclick="jsFunc(this);">Add</button>
                        <span id="spnAdded" style="display: none">Added</span>
                    </td>
                </tr>
                <?php
            }
            ?>
        </table>
    </div>
</div>