<?php
include_once COMMON.'class.common.meal.php';
include_once UTILITY.'class.util.php';


Class MenuDAO
{

    private $_DB;
    private $_Menu;
    private $_Food;

    public function __construct()
    {

        $this->_DB = DBUtil::getInstance();
        $this->_Menu=new Menu();
        $this->_Food=new Food();

    }

    public function createMenu($Menu){
        $ID=$Menu->getMenuId();
        $MealType=$Menu->getMenuMealTypeId();
        $HousingID=$Menu->getMenuHousingId();

        $SQL="INSERT INTO mms_menu VALUES('$ID','$MealType','$HousingID',0)";
        $SQL=$this->_DB->doQuery($SQL);

        $Result=new Result();
        $Result->setIsSuccess(1);
        $Result->setResultObject($SQL);

        return $Result;

    }

    public function getMenuByHallId($Hall){
        $MenuList=array();
        $SQL="SELECT * FROM mms_menu WHERE mms_menu.housing_id='".$Hall."'";

        $this->_DB->doQuery($SQL);
        $rows=$this->_DB->getAllRows();

        foreach ($rows as $row) {
            $this->_Menu=new Menu();
            $this->_Menu->setMenuId($row['id']);
            $this->_Menu->setMenuMealTypeId($row['mealType_id']);
            $this->_Menu->setMenuHousingId($row['housing_id']);
            $this->_Menu->setMenuTotalCost($row['totalCost']);

            $MenuList[]=$this->_Menu;
        }

        $Result=new Result();
        $Result->setIsSuccess(1);
        $Result->setResultObject($MenuList);

        return $Result;
    }

    public function deleteMenu($Menu){
        $SQL="DELETE FROM mms_menu WHERE mms_menu.id='".$Menu->getMenuId()."'";
        $SQL=$this->_DB->doQuery($SQL);

        $Result=new Result();
        $Result->setIsSuccess(1);
        $Result->setResultObject($SQL);

        return $Result;
    }

    public function getFoodByMenuId($Menu){
        $FoodList=array();
        $SQL="SELECT * FROM mms_food_menu WHERE mms_food_menu.menu_id='".$Menu."'";

        $this->_DB->doQuery($SQL);
        $rows=$this->_DB->getAllRows();

        foreach ($rows as $row) {
            $this->_Food=new Food();
            $this->_Food->setFoodId($row['food_id']);

            $FoodList[]=$this->_Food;
        }

        $Result=new Result();
        $Result->setIsSuccess(1);
        $Result->setResultObject($FoodList);

        return $Result;
    }

    public function addTotalCost($Menu,$totalCost){
        $SQL="UPDATE mms_menu SET mms_menu.totalCost='$totalCost' WHERE mms_menu.id='$Menu'";
        $SQL=$this->_DB->doQuery($SQL);

        $Result=new Result();
        $Result->setIsSuccess(1);
        $Result->setResultObject($SQL);

        return $Result;
    }

    public function createOffer($Offer){
        $ID=$Offer->getOfferId();
        $HousingID=$Offer->getHousingId();
        $MenuID=$Offer->getOfferMenuId();
        $Date=$Offer->getOfferDate();

        $SQL="INSERT INTO mms_offer VALUES('$ID','$HousingID','$MenuID','$Date')";
        $SQL=$this->_DB->doQuery($SQL);

        $Result=new Result();
        $Result->setIsSuccess(1);
        $Result->setResultObject($SQL);

        return $Result;
    }
}