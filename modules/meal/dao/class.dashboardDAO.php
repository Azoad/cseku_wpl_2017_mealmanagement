<?php
include_once COMMON.'class.common.meal.php';
include_once UTILITY.'class.util.php';


Class DashboardDAO
{

    private $_DB;
    private $_Offer;

    public function __construct()
    {

        $this->_DB = DBUtil::getInstance();
        $this->_Offer = new Offer();

    }

    public function getAllOffer(){
        $OfferList=array();
        $SQL="SELECT * FROM mms_offer";
        $this->_DB->doQuery($SQL);
        $rows=$this->_DB->getAllRows();
        foreach ($rows as $row){
            $this->_Offer=new Offer();
            $this->_Offer->setOfferId($row['id']);
            $this->_Offer->setHousingId($row['housing_id']);
            $this->_Offer->setOfferMenuId($row['menu_id']);
            $this->_Offer->setOfferDate($row['offerDate']);

            $OfferList[]=$this->_Offer;
        }

        $Result=new Result();
        $Result->setIsSuccess(1);
        $Result->setResultObject($OfferList);

        return $Result;
    }
}
