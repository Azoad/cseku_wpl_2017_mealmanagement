<?php
include_once COMMON.'class.common.meal.php';
include_once UTILITY.'class.util.php';


Class OrderListDAO
{

    private $_DB;
    private $_Order;
    private $_User;

    public function __construct()
    {

        $this->_DB = DBUtil::getInstance();
        $this->_Order=new Order();
        $this->_User=new User();
    }

    public function getOrderByHallId($Hall){
        $Mealtype="";
        if (isset($_POST['mealType'])){
            $Mealtype=$_POST['mealType'];
        }
        $OrderDate="";
        if (isset($_POST['orderDate'])){
            $OrderDate=$_POST['orderDate'];
        }

        
        $OrderList=array();
        $SQL="SELECT * FROM mms_order INNER JOIN mms_menu ON mms_order.menu_id=mms_menu.id WHERE mms_order.housing_id='".$Hall."' AND mms_order.orderDate LIKE '%".$OrderDate."%' AND mms_menu.mealType_id LIKE '%".$Mealtype."%'";
        $this->_DB->doQuery($SQL);

        $rows=$this->_DB->getAllRows();

        foreach ($rows as $row){
            $this->_Order=new Order();
            $this->_Order->setOrderID($row['id']);
            $this->_Order->setOrderMenuId($row['menu_id']);
            $this->_Order->setOrderUserId($row['user_id']);
            $this->_Order->setOrderHousingId($row['housing_id']);
            $this->_Order->setOrderDate($row['orderDate']);

            $OrderList[]=$this->_Order;
        }

        $Result=new Result();
        $Result->setIsSuccess(1);
        $Result->setResultObject($OrderList);

        return $Result;
    }

    public function getUserNameById($User){
        $SQL="SELECT * FROM tbl_user WHERE tbl_user.ID='".$User."'";
        $this->_DB->doQuery($SQL);

        $row=$this->_DB->getTopRow();

        $this->_User=new User();
        $this->_User->setID($row['ID']);
        $this->_User->setFirstName($row['FirstName']);
        $this->_User->setLastName($row['LastName']);

        $Result=new Result();
        $Result->setIsSuccess(1);
        $Result->setResultObject($this->_User);

        return $Result;
    }
}