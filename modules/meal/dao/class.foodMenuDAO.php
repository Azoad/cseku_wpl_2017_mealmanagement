<?php
include_once COMMON.'class.common.meal.php';
include_once UTILITY.'class.util.php';


Class FoodMenuDAO
{

    private $_DB;
    private $_FoodMenu;
    private $_Food;

    public function __construct()
    {

        $this->_DB = DBUtil::getInstance();
        $this->_FoodMenu = new FoodMenu();
        $this->_Food=new Food();

    }
    public function getFoodByHallId($Hall,$Menu){
        $FoodList=array();
        $SQL="SELECT * FROM mms_food WHERE mms_food.housing_id='".$Hall."' AND mms_food.id NOT IN(SELECT food_id from mms_food_menu WHERE menu_id='".$Menu."')";

        $this->_DB->doQuery($SQL);
        $rows=$this->_DB->getAllRows();

        foreach ($rows as $row) {
            $this->_Food=new Food();
            $this->_Food->setFoodId($row['id']);
            $this->_Food->setFoodName($row['foodName']);
            $this->_Food->setFoodCost($row['foodCost']);
            $this->_Food->setFoodHousingId($row['housing_id']);

            $FoodList[]=$this->_Food;
        }

        $Result=new Result();
        $Result->setIsSuccess(1);
        $Result->setResultObject($FoodList);

        return $Result;
    }
}