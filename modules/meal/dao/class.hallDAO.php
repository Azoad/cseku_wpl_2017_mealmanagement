<?php
include_once COMMON.'class.common.meal.php';
include_once UTILITY.'class.util.php';


Class HallDAO
{

    private $_DB;
    private $_Housing;

    public function __construct()
    {

        $this->_DB = DBUtil::getInstance();
        $this->_Housing = new Housing();

    }

    public function getAllHall(){
        $HousingList=array();

        $SQL="SELECT * FROM mms_housing";
        $this->_DB->doQuery($SQL);
        $rows=$this->_DB->getAllRows();

        foreach ($rows as $row){
            $this->_Housing=new Housing();
            $this->_Housing->setHousingId($row['id']);
            $this->_Housing->setHousingName($row['housingName']);

            $HousingList[]=$this->_Housing;
        }

        $Result=new Result();
        $Result->setIsSuccess(1);
        $Result->setResultObject($HousingList);

        return $Result;
    }

    public function getHallNameById($Hall){
        $SQL="SELECT * FROM mms_housing WHERE mms_housing.id='".$Hall."'";
        $this->_DB->doQuery($SQL);
        $row=$this->_DB->getTopRow();

        $this->_Housing=new Housing();
        $this->_Housing->setHousingId($row['id']);
        $this->_Housing->setHousingName($row['housingName']);

        $Result=new Result();
        $Result->setIsSuccess(1);
        $Result->setResultObject($this->_Housing);

        return $Result;
    }
}