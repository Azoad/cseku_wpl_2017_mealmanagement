<?php
include_once COMMON.'class.common.meal.php';
include_once UTILITY.'class.util.php';


Class OrderDAO
{

    private $_DB;
    private $_Offer;
    private $_Menu;
    private $_MealType;
    private $_FoodMenu;
    private $_Order;

    public function __construct()
    {

        $this->_DB = DBUtil::getInstance();
        $this->_Offer=new Offer();
        $this->_Menu=new Menu();
        $this->_MealType=new MealType();
        $this->_FoodMenu=new FoodMenu();
        $this->_Order=new Order();
    }

    public function getOfferByHallId($Hall){
        $OfferList=array();
        $SQL="SELECT * FROM mms_offer WHERE mms_offer.housing_id='".$Hall."'";
        $this->_DB->doQuery($SQL);

        $rows=$this->_DB->getAllRows();

        foreach ($rows as $row){
            $this->_Offer=new Offer();
            $this->_Offer->setOfferId($row['id']);
            $this->_Offer->setHousingId($row['housing_id']);
            $this->_Offer->setOfferMenuId($row['menu_id']);
            $this->_Offer->setOfferDate($row['offerDate']);

            $OfferList[]=$this->_Offer;
        }

        $Result=new Result();
        $Result->setIsSuccess(1);
        $Result->setResultObject($OfferList);

        return $Result;
    }

    public function getMenuByMenuId($Menu){
        $SQL="SELECT * FROM mms_menu WHERE mms_menu.id='".$Menu."'";
        $this->_DB->doQuery($SQL);

        $row=$this->_DB->getTopRow();

        $this->_Menu=new Menu();
        $this->_Menu->setMenuId($row['id']);
        $this->_Menu->setMenuMealTypeId($row['mealType_id']);
        $this->_Menu->setMenuHousingId($row['housing_id']);
        $this->_Menu->setMenuTotalCost($row['totalCost']);

        $Result=new Result();
        $Result->setIsSuccess(1);
        $Result->setResultObject($this->_Menu);

        return $Result;
    }

    public function getMealTypeByMealTypeId($MealType){
        $SQL="SELECT * FROM mms_mealtype WHERE mms_mealtype.id='".$MealType."'";

        $this->_DB->doQuery($SQL);

        $row=$this->_DB->getTopRow();

        $this->_MealType=new MealType();
        $this->_MealType->setMealTypeId($row['id']);
        $this->_MealType->setMealTypeName($row['mealTypeName']);
        $this->_MealType->setMealTypeTime($row['time']);

        $Result=new Result();
        $Result->setIsSuccess(1);
        $Result->setResultObject($this->_MealType);

        return $Result;
    }

    public function getFoodIdByMenuId($Menu){
        $FoodMenuList=array();
        $SQL="SELECT * FROM mms_food_menu WHERE mms_food_menu.menu_id='".$Menu."'";
        $this->_DB->doQuery($SQL);

        $rows=$this->_DB->getAllRows();

        foreach ($rows as $row){
            $this->_FoodMenu=new FoodMenu();
            $this->_FoodMenu->setFoodMenuFoodId($row['food_id']);
            $this->_FoodMenu->setFoodMenuMenuId($row['menu_id']);

            $FoodMenuList[]=$this->_FoodMenu;
        }

        $Result=new Result();
        $Result->setIsSuccess(1);
        $Result->setResultObject($FoodMenuList);

        return $Result;
    }

    public function createOrder($Order){
        $ID=$Order->getOrderId();
        $MenuID=$Order->getOrderMenuId();
        $UserID=$Order->getOrderUserId();
        $HousingID=$Order->getOrderHousingId();

        $SQL="INSERT INTO mms_order VALUES ('$ID','$MenuID','$UserID','$HousingID',CURRENT_TIMESTAMP)";

        $SQL=$this->_DB->doQuery($SQL);

        $Result=new Result();
        $Result->setIsSuccess(1);
        $Result->setResultObject($SQL);

        return $Result;
    }
}