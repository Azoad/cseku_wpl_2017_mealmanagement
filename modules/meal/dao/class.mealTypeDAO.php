<?php
include_once COMMON.'class.common.meal.php';
include_once UTILITY.'class.util.php';


Class MealTypeDAO
{

    private $_DB;
    private $_MealType;

    public function __construct()
    {

        $this->_DB = DBUtil::getInstance();
        $this->_MealType = new MealType();

    }

    public function createMealType($MealType){
        $ID=$MealType->getMealTypeId();
        $Name=$MealType->getMealTypeName();
        $Time=$MealType->getMealTypeTime();

        $SQL="INSERT INTO mms_mealtype VALUES('$ID','$Name','$Time')";
        $SQL=$this->_DB->doQuery($SQL);

        $Result=new Result();
        $Result->setIsSuccess(1);
        $Result->setResultObject($SQL);

        return $Result;

    }

    public function getAllMealType(){
        $MealTypeList=array();

        $SQL="SELECT * FROM mms_mealtype";
        $this->_DB->doQuery($SQL);
        $rows=$this->_DB->getAllRows();

        foreach ($rows as $row){
            $this->_MealType=new MealType();
            $this->_MealType->setMealTypeId($row['id']);
            $this->_MealType->setMealTypeName($row['mealTypeName']);
            $this->_MealType->setMealTypeTime($row['time']);

            $MealTypeList[]=$this->_MealType;
        }

        $Result=new Result();
        $Result->setIsSuccess(1);
        $Result->setResultObject($MealTypeList);

        return $Result;
    }

    public function getMealTypeById($MealType){
        $SQL="SELECT * FROM mms_mealtype WHERE mms_mealtype.id='".$MealType->getMealTypeId()."'";
        $this->_DB->doQuery($SQL);
        $row=$this->_DB->getTopRow();

        $this->_MealType=new MealType();
        $this->_MealType->setMealTypeId($row['id']);
        $this->_MealType->setMealTypeName($row['mealTypeName']);
        $this->_MealType->setMealTypeTime($row['time']);

        $Result=new Result();
        $Result->setIsSuccess(1);
        $Result->setResultObject($this->_MealType);

        return $Result;
    }

    public function updateMealType($MealType){
        $SQL="UPDATE mms_mealtype SET mms_mealtype.mealTypeName='".$MealType->getMealTypeName()."',mms_mealtype.time='".$MealType->getMealTypeTime()."'
        WHERE mms_mealtype.id='".$MealType->getMealTypeId()."'";
        $SQL=$this->_DB->doQuery($SQL);

        $Result=new Result();
        $Result->getIsSuccess(1);
        $Result->setResultObject($SQL);

        return $Result;
    }

    public function deleteMealType($MealType){
        $SQL="DELETE FROM mms_mealtype WHERE mms_mealtype.id='".$MealType->getMealTypeId()."'";
        $SQL=$this->_DB->doQuery($SQL);

        $Result=new Result();
        $Result->setIsSuccess(1);
        $Result->setResultObject($SQL);

        return $Result;
    }
}
