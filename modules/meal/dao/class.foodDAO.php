<?php
include_once COMMON.'class.common.meal.php';
include_once UTILITY.'class.util.php';


Class FoodDAO
{

    private $_DB;
    private $_Food;

    public function __construct()
    {

        $this->_DB = DBUtil::getInstance();
        $this->_Food = new Food();

    }

    public function createFood($Food){
        $ID=$Food->getFoodId();
        $Name=$Food->getFoodName();
        $Cost=$Food->getFoodCost();
        $HousingID=$Food->getFoodHousingId();

        $SQL="INSERT INTO mms_food VALUES('$ID','$Name','$Cost','$HousingID')";
        $SQL=$this->_DB->doQuery($SQL);

        $Result=new Result();
        $Result->setIsSuccess(1);
        $Result->setResultObject($SQL);

        return $Result;

    }

    public function getFoodByHallId($Hall){
        $FoodList=array();
        $SQL="SELECT * FROM mms_food WHERE mms_food.housing_id='".$Hall."'";

        $this->_DB->doQuery($SQL);
        $rows=$this->_DB->getAllRows();

        foreach ($rows as $row) {
            $this->_Food=new Food();
            $this->_Food->setFoodId($row['id']);
            $this->_Food->setFoodName($row['foodName']);
            $this->_Food->setFoodCost($row['foodCost']);
            $this->_Food->setFoodHousingId($row['housing_id']);

            $FoodList[]=$this->_Food;
        }

        $Result=new Result();
        $Result->setIsSuccess(1);
        $Result->setResultObject($FoodList);

        return $Result;
    }

    public function getFoodById($Food){
        $SQL="SELECT * FROM mms_food WHERE mms_food.id='".$Food->getFoodId()."'";
        $this->_DB->doQuery($SQL);
        $row=$this->_DB->getTopRow();

        $this->_Food=new Food();
        $this->_Food->setFoodId($row['id']);
        $this->_Food->setFoodName($row['foodName']);
        $this->_Food->setFoodCost($row['foodCost']);
        $this->_Food->setFoodHousingId($row['housing_id']);

        $Result=new Result();
        $Result->setIsSuccess(1);
        $Result->setResultObject($this->_Food);

        return $Result;
    }

    public function updateFood($Food){
        $SQL="UPDATE mms_food SET mms_food.foodName='".$Food->getFoodName()."',mms_food.foodCost='".$Food->getFoodCost()."',
        mms_food.housing_id='".$Food->getFoodHousingId()."' WHERE mms_food.id='".$Food->getFoodId()."'";
        $SQL=$this->_DB->doQuery($SQL);

        $Result=new Result();
        $Result->getIsSuccess(1);
        $Result->setResultObject($SQL);

        return $Result;
    }

    public function deleteFood($Food){
        $SQL="DELETE FROM mms_food WHERE mms_food.id='".$Food->getFoodId()."'";
        $SQL=$this->_DB->doQuery($SQL);

        $Result=new Result();
        $Result->setIsSuccess(1);
        $Result->setResultObject($SQL);

        return $Result;
    }
}