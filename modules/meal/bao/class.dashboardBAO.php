<?php

include_once UTILITY.'class.util.php';
include_once MODULES_MEAL.'dao/class.dashboardDAO.php';


/*
	Discussion Business Object
*/
Class DashboardBAO
{
    private $_DashboardDAO;

    public function __construct()
    {

        $this->_DashboardDAO = new DashboardDAO();

    }

    public function getAllOffer(){
        $Result=$this->_DashboardDAO->getAllOffer();
        if(!$Result->getIsSuccess()){
            $Result->setResultObject('Failed');
        }

        return $Result;
    }
}