<?php

include_once UTILITY.'class.util.php';
include_once MODULES_MEAL.'dao/class.foodMenuDAO.php';


/*
	Discussion Business Object
*/
Class FoodMenuBAO
{
    private $_FoodMenuDAO;

    public function __construct()
    {

        $this->_FoodMenuDAO = new FoodMenuDAO();

    }

    public function getFoodByHallId($Hall,$Menu){
        $Result=$this->_FoodMenuDAO->getFoodByHallId($Hall,$Menu);

        if (!$Result->getIsSuccess()){
            $Result->setResultObject("Failure");
        }

        return $Result;
    }
}