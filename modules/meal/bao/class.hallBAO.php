<?php

include_once UTILITY.'class.util.php';
include_once MODULES_MEAL.'dao/class.hallDAO.php';


/*
	Discussion Business Object
*/
Class HallBAO
{
    private $_HallDAO;

    public function __construct()
    {

        $this->_HallDAO = new HallDAO();

    }

    public function getAllHall(){
        $Result=$this->_HallDAO->getAllHall();

        if (!$Result->getIsSuccess()){
            $Result->setResultObject("Database failure in HallDAO.getAllHall()");
        }

        return $Result;
    }

    public function getHallNameById($Hall){
        $Result=$this->_HallDAO->getHallNameById($Hall);

        if (!$Result->getIsSuccess()){
            $Result->setResultObject("Database failure in HallDAO.getAllHall()");
        }

        return $Result;
    }
}