<?php

include_once UTILITY.'class.util.php';
include_once MODULES_MEAL.'dao/class.menuDAO.php';


/*
	Discussion Business Object
*/
Class MenuBAO
{
    private $_MenuDAO;

    public function __construct()
    {

        $this->_MenuDAO = new MenuDAO();

    }

    public function createMenu($Menu){
        $Result=$this->_MenuDAO->createMenu($Menu);

        if (!$Result->getIsSuccess()){
            $Result->setResultObject("Database failure in FoodDao.createFood($Menu)");
        }

        return $Result;
    }

    public function getMenuByHallId($Hall){
        $Result=$this->_MenuDAO->getMenuByHallId($Hall);

        if (!$Result->getIsSuccess()){
            $Result->setResultObject("Failure");
        }

        return $Result;
    }


    public function deleteMenu($Menu){
        $Result=$this->_MenuDAO->deleteMenu($Menu);

        if (!$Result->getIsSuccess()){
            $Result->setResultObject("Failure");
        }

        return $Result;
    }

    public function getFoodByMenuId($Menu)
    {
        $Result = $this->_MenuDAO->getFoodByMenuId($Menu);

        if (!$Result->getIsSuccess()) {
            $Result->setResultObject("Failure");
        }

        return $Result;
    }

    public function addTotalCost($Menu,$totalCost){
        $Result=$this->_MenuDAO->addTotalCost($Menu,$totalCost);

        if (!$Result->getIsSuccess()){
            $Result->setResultObject("Failed");
        }

        return $Result;
    }

    public function createOffer($Offer){
        $Result=$this->_MenuDAO->createOffer($Offer);

        if (!$Result->getIsSuccess()){
            $Result->setResultObject("Failed");
        }
        return $Result;
    }
}