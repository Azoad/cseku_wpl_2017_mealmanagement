<?php

include_once UTILITY.'class.util.php';
include_once MODULES_MEAL.'dao/class.orderDetailsDAO.php';


/*
	Discussion Business Object
*/
Class OrderDetailsBAO
{
    private $_OrderDetailsDAO;

    public function __construct()
    {

        $this->_OrderDetailsDAO = new OrderDetailsDAO();

    }

    public function getOrderByUserId($User){
        $Result=$this->_OrderDetailsDAO->getOrderByUserId($User);

        if (!$Result->getIsSuccess()){
            $Result->setResultObject('Failed');
        }

        return $Result;
    }

    public function getUserNameById($User){
        $Result=$this->_OrderListDAO->getUserNameById($User);

        if (!$Result->getIsSuccess()){
            $Result->setResultObject('Failed');
        }

        return $Result;
    }
}