<?php

include_once UTILITY.'class.util.php';
include_once MODULES_MEAL.'dao/class.orderListDAO.php';


/*
	Discussion Business Object
*/
Class OrderListBAO
{
    private $_OrderListDAO;

    public function __construct()
    {

        $this->_OrderListDAO = new OrderListDAO();

    }

    public function getOrderByHallId($Hall){
        $Result=$this->_OrderListDAO->getOrderByHallId($Hall);

        if (!$Result->getIsSuccess()){
            $Result->setResultObject('Failed');
        }

        return $Result;
    }

    public function getUserNameById($User){
        $Result=$this->_OrderListDAO->getUserNameById($User);

        if (!$Result->getIsSuccess()){
            $Result->setResultObject('Failed');
        }

        return $Result;
    }
}