<?php

include_once UTILITY.'class.util.php';
include_once MODULES_MEAL.'dao/class.foodDAO.php';


/*
	Discussion Business Object
*/
Class FoodBAO
{
    private $_FoodDAO;

    public function __construct()
    {

        $this->_FoodDAO = new FoodDAO();

    }

    public function createFood($Food){
        $Result=$this->_FoodDAO->createFood($Food);

        if (!$Result->getIsSuccess()){
            $Result->setResultObject("Database failure in FoodDao.createFood($Food)");
        }

        return $Result;
    }

    public function getFoodByHallId($Hall){
        $Result=$this->_FoodDAO->getFoodByHallId($Hall);

        if (!$Result->getIsSuccess()){
            $Result->setResultObject("Failure");
        }

        return $Result;
    }

    public function getFoodById($Food){
        $Result=$this->_FoodDAO->getFoodById($Food);

        if (!$Result->getIsSuccess()){
            $Result->setResultObject("Failure");
        }

        return $Result;
    }

    public function updateFood($Food){
        $Result=$this->_FoodDAO->updateFood($Food);

        if (!$Result->getIsSuccess()){
            $Result->setResultObject("Failure");
        }

        return $Result;
    }

    public function deleteFood($Food){
        $Result=$this->_FoodDAO->deleteFood($Food);

        if (!$Result->getIsSuccess()){
            $Result->setResultObject("Failure");
        }

        return $Result;
    }
}