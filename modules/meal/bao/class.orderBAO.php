<?php

include_once UTILITY.'class.util.php';
include_once MODULES_MEAL.'dao/class.orderDAO.php';


/*
	Discussion Business Object
*/
Class OrderBAO
{
    private $_OrderDAO;

    public function __construct()
    {

        $this->_OrderDAO = new OrderDAO();

    }

    public function getOfferByHallId($Hall){
        $Result=$this->_OrderDAO->getOfferByHallId($Hall);

        if (!$Result->getIsSuccess()){
            $Result->setResultObject('Failed');
        }

        return $Result;
    }

    public function getMenuByMenuId($Menu){
        $Result=$this->_OrderDAO->getMenuByMenuId($Menu);

        if (!$Result->getIsSuccess()){
            $Result->setResultObject('Failed');
        }

        return $Result;
    }

    public function getMealTypeByMealTypeId($MealType){
        $Result=$this->_OrderDAO->getMealTypeByMealTypeId($MealType);

        if (!$Result->getIsSuccess()){
            $Result->setResultObject('Failed');
        }

        return $Result;
    }

    public function getFoodIdByMenuId($Menu){
        $Result=$this->_OrderDAO->getFoodIdByMenuId($Menu);
        if (!$Result->getIsSuccess()){
            $Result->setResultObject('Failed');
        }

        return $Result;
    }

    public function createOrder($Order){
        $Result=$this->_OrderDAO->createOrder($Order);

        if (!$Result->getIsSuccess()){
            $Result->setResultObject('Failed');
        }

        return $Result;
    }
}