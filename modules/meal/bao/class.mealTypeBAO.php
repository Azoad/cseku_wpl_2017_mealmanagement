<?php

include_once UTILITY.'class.util.php';
include_once MODULES_MEAL.'dao/class.mealTypeDAO.php';


/*
	Discussion Business Object
*/
Class MealTypeBAO
{
    private $_MealTypeDAO;

    public function __construct()
    {

        $this->_MealTypeDAO = new MealTypeDAO();

    }
    public function createMealType($MealType){
        $Result=$this->_MealTypeDAO->createMealType($MealType);

        if (!$Result->getIsSuccess()){
            $Result->setResultObject("Database failure in FoodDao.createFood($MealType)");
        }

        return $Result;
    }
    public function getAllMealType(){
        $Result=$this->_MealTypeDAO->getAllMealType();

        if (!$Result->getIsSuccess()){
            $Result->setResultObject("Database failure in HallDAO.getAllHall()");
        }

        return $Result;
    }

    public function getMealTypeById($MealType){
        $Result=$this->_MealTypeDAO->getMealTypeById($MealType);

        if (!$Result->getIsSuccess()){
            $Result->setResultObject("Failure");
        }

        return $Result;
    }

    public function updateMealType($MealType){
        $Result=$this->_MealTypeDAO->updateMealType($MealType);

        if (!$Result->getIsSuccess()){
            $Result->setResultObject("Failure");
        }

        return $Result;
    }

    public function deleteMealType($MealType){
        $Result=$this->_MealTypeDAO->deleteMealType($MealType);

        if (!$Result->getIsSuccess()){
            $Result->setResultObject("Failure");
        }

        return $Result;
    }
}