<?php
class Food{
    private $_foodId;
    private $_foodName;
    private $_foodCost;
    private $_foodHousingId;

    //Id
    public function getFoodId(){
        return $this->_foodId;
    }

    public function setFoodId($foodId){
        $this->_foodId=$foodId;
    }

    //Name
    public function getFoodName(){
        return $this->_foodName;
    }

    public function setFoodName($foodName){
        $this->_foodName=$foodName;
    }

    //Cost
    public function getFoodCost(){
        return $this->_foodCost;
    }

    public function setFoodCost($foodCost){
        $this->_foodCost=$foodCost;
    }

    //FoodHousingId
    public function getFoodHousingId(){
        return $this->_foodHousingId;
    }

    public function setFoodHousingId($foodHousingId){
        $this->_foodHousingId=$foodHousingId;
    }

}

class Menu{
    private $_menuId;
    private $_menuMealTypeId;
    private $_menuHousingId;
    private $_menuTotalCost;

    //Id
    public function getMenuId(){
        return $this->_menuId;
    }

    public function setMenuId($menuId){
        $this->_menuId=$menuId;
    }

    //MealTypeId
    public function getMenuMealTypeId(){
        return $this->_menuMealTypeId;
    }

    public function setMenuMealTypeId($menuMealTypeId){
        $this->_menuMealTypeId=$menuMealTypeId;
    }

    //HousingId
    public function getMenuHousingId(){
        return $this->_menuHousingId;
    }

    public function setMenuHousingId($menuHousingId){
        $this->_menuHousingId=$menuHousingId;
    }

    //TotalCost
    public function getMenuTotalCost(){
        return $this->_menuTotalCost;
    }

    public function setMenuTotalCost($menuTotalCost){
        $this->_menuTotalCost=$menuTotalCost;
    }

}

class FoodMenu{
    private $_foodMenuFoodId;
    private $_foodMenuMenuId;
    
    //FoodId
    public function getFoodMenuFoodId(){
        return $this->_foodMenuFoodId;
    }
    
    public function setFoodMenuFoodId($foodMenuFoodId){
        $this->_foodMenuFoodId=$foodMenuFoodId;
    }
    
    //MenuId
    public function getFoodMenuMenuId(){
        return $this->_foodMenuMenuId;
    }
    
    public function setFoodMenuMenuId($foodMenuMenuId){
        $this->_foodMenuMenuId=$foodMenuMenuId;
    }
    
}

class Housing{
    private $_housingId;
    private $_housingName;

    //Id
    public function getHousingId(){
        return $this->_housingId;
    }

    public function setHousingId($housingId){
        $this->_housingId=$housingId;
    }

    //Name
    public function getHousingName(){
        return $this->_housingName;
    }

    public function setHousingName($housingName){
        $this->_housingName=$housingName;
    }

}

class MealType{
    private $_mealTypeId;
    private $_mealTypeName;
    private $_mealTypeTime;
    
    //Id
    public function getMealTypeId(){
        return $this->_mealTypeId;
    }
    
    public function setMealTypeId($mealTypeId){
        $this->_mealTypeId=$mealTypeId;
    }
    
    //Name
    public function getMealTypeName(){
        return $this->_mealTypeName;
    }
    
    public function setMealTypeName($mealTypeName){
        $this->_mealTypeName=$mealTypeName;
    }
    
    //Time
    public function getMealTypeTime(){
        return $this->_mealTypeTime;
    }
    
    public function setMealTypeTime($mealTypeTime){
        $this->_mealTypeTime=$mealTypeTime;
    }
    
}

class Offer{
    private $_offerId;
    private $_housingId;
    private $_offerMenuId;
    private $_offerDate;
    
    //Id
    public function getOfferId(){
        return $this->_offerId;
    }
    
    public function setOfferId($offerId){
        $this->_offerId=$offerId;
    }

    //HousingId
    public function getHousingId(){
        return $this->_housingId;
    }

    public function setHousingId($housingId){
        $this->_housingId=$housingId;
    }

    //MenuId
    public function getOfferMenuId(){
        return $this->_offerMenuId;
    }
    
    public function setOfferMenuId($offerMenuId){
        $this->_offerMenuId=$offerMenuId;
    }
    
    //Date
    public function getOfferDate(){
        return $this->_offerDate;
    }
    
    public function setOfferDate($offerDate){
        $this->_offerDate=$offerDate;
    }
    
}

class Order{
    private $_orderId;
    private $_orderMenuId;
    private $_orderUserId;
    private $_orderHousingId;
    private $_orderDate;

    //Id
    public function getOrderID(){
        return $this->_orderId;
    }
    
    public function setOrderID($orderId){
        $this->_orderId=$orderId;
    }
    
    //MenuId
    public function getOrderMenuId(){
        return $this->_orderMenuId;
    }
    
    public function setOrderMenuId($orderMenuId){
        $this->_orderMenuId=$orderMenuId;
    }
    
    //UserId
    public function getOrderUserId(){
        return $this->_orderUserId;
    }
    
    public function setOrderUserId($orderUserId){
        $this->_orderUserId=$orderUserId;
    }
    
    //HousingId
    public function getOrderHousingId(){
        return $this->_orderHousingId;
    }
    
    public function setOrderHousingId($orderHousingId){
        $this->_orderHousingId=$orderHousingId;
    }
    
    //Date
    public function getOrderDate(){
        return $this->_orderDate;
    }
    
    public function setOrderDate($orderDate){
        $this->_orderDate=$orderDate;
    }
    
    
}
?>