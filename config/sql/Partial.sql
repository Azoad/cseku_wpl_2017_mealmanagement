-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 07, 2017 at 04:18 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mealmanagement`
--

-- --------------------------------------------------------

--
-- Table structure for table `hms_hall`
--

CREATE TABLE `hms_hall` (
  `id` varchar(40) NOT NULL,
  `name` varchar(40) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `provost` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mms_food`
--

CREATE TABLE `mms_food` (
  `id` varchar(40) NOT NULL,
  `foodName` varchar(256) NOT NULL,
  `foodCost` double NOT NULL,
  `housing_id` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mms_food`
--

INSERT INTO `mms_food` (`id`, `foodName`, `foodCost`, `housing_id`) VALUES
('{038B23C8-9CC2-462E-9845-DACE73F3655F}', 'Chicken Fry', 50, '1'),
('{08DDB25E-5F9C-4A1D-85CD-D8892A11303D}', 'Pizza', 350, '2'),
('{456B983D-2B94-4FCA-A4EB-A7A20C0F50AC}', 'Shawarma', 100, '2'),
('{4BD0FF53-78AB-433B-B711-B82C6A6CBB8F}', 'Raw Tea', 6, '2'),
('{98291889-48AF-410D-A144-A17DEDD2698C}', '7up', 20, '2'),
('{9A9438DA-95B1-48A3-AD05-60B2E6298C87}', 'Burger', 110, '2'),
('{AF27B395-E592-48E8-9CE0-A2F48EE81C35}', 'Berger', 80, '1'),
('{B3073EBC-C1A6-4510-89BD-48CB0B1C6694}', 'Fried Rice', 200, '1'),
('{DD7018D6-3A48-443E-A9F0-5E1912CF2EA9}', 'Coffee', 40, '1'),
('{E2A59F06-DD57-4C5B-B12D-FA8463FA21F2}', 'Bun', 10, '2'),
('{E7999A4C-8731-4D36-9590-0A839667ECE7}', 'Pizza', 100, '1');

-- --------------------------------------------------------

--
-- Table structure for table `mms_food_menu`
--

CREATE TABLE `mms_food_menu` (
  `food_id` varchar(40) NOT NULL,
  `menu_id` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mms_food_menu`
--

INSERT INTO `mms_food_menu` (`food_id`, `menu_id`) VALUES
('{038B23C8-9CC2-462E-9845-DACE73F3655F}', '{C5621510-ADBD-4878-B5A3-AE8E654DEAEC}'),
('{B3073EBC-C1A6-4510-89BD-48CB0B1C6694}', '{C5621510-ADBD-4878-B5A3-AE8E654DEAEC}'),
('{038B23C8-9CC2-462E-9845-DACE73F3655F}', '{10646EFC-301B-4596-858A-1BAC2DF3F351}'),
('{E7999A4C-8731-4D36-9590-0A839667ECE7}', '{10646EFC-301B-4596-858A-1BAC2DF3F351}'),
('{DD7018D6-3A48-443E-A9F0-5E1912CF2EA9}', '{47E1EB6F-34F8-4BA6-9699-C0967485BB2A}'),
('{AF27B395-E592-48E8-9CE0-A2F48EE81C35}', '{47E1EB6F-34F8-4BA6-9699-C0967485BB2A}'),
('{E2A59F06-DD57-4C5B-B12D-FA8463FA21F2}', '{7758C07D-4D1B-41E3-BAE0-642F932EA8CF}'),
('{4BD0FF53-78AB-433B-B711-B82C6A6CBB8F}', '{7758C07D-4D1B-41E3-BAE0-642F932EA8CF}'),
('{08DDB25E-5F9C-4A1D-85CD-D8892A11303D}', '{06B03531-8BAA-4F38-BC22-958CDEB1F35C}'),
('{98291889-48AF-410D-A144-A17DEDD2698C}', '{06B03531-8BAA-4F38-BC22-958CDEB1F35C}'),
('{456B983D-2B94-4FCA-A4EB-A7A20C0F50AC}', '{06B03531-8BAA-4F38-BC22-958CDEB1F35C}'),
('{9A9438DA-95B1-48A3-AD05-60B2E6298C87}', '{2BF76DEA-D437-4037-A73F-AED5A4348BED}'),
('{98291889-48AF-410D-A144-A17DEDD2698C}', '{2BF76DEA-D437-4037-A73F-AED5A4348BED}'),
('{AF27B395-E592-48E8-9CE0-A2F48EE81C35}', '{10646EFC-301B-4596-858A-1BAC2DF3F351}'),
('{B3073EBC-C1A6-4510-89BD-48CB0B1C6694}', '{10646EFC-301B-4596-858A-1BAC2DF3F351}'),
('{038B23C8-9CC2-462E-9845-DACE73F3655F}', '{8A566225-FC8B-4DB0-9DED-5D2AED0352AD}'),
('{AF27B395-E592-48E8-9CE0-A2F48EE81C35}', '{8A566225-FC8B-4DB0-9DED-5D2AED0352AD}'),
('{B3073EBC-C1A6-4510-89BD-48CB0B1C6694}', '{8A566225-FC8B-4DB0-9DED-5D2AED0352AD}');

-- --------------------------------------------------------

--
-- Table structure for table `mms_housing`
--

CREATE TABLE `mms_housing` (
  `id` varchar(40) NOT NULL,
  `housingName` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mms_housing`
--

INSERT INTO `mms_housing` (`id`, `housingName`) VALUES
('1', 'Khan Jahan Ali Hall'),
('2', 'Khan Bahadur Ahsanullah Hall');

-- --------------------------------------------------------

--
-- Table structure for table `mms_mealtype`
--

CREATE TABLE `mms_mealtype` (
  `id` varchar(40) NOT NULL,
  `mealTypeName` varchar(256) NOT NULL,
  `time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mms_mealtype`
--

INSERT INTO `mms_mealtype` (`id`, `mealTypeName`, `time`) VALUES
('{2ADCE5CC-28EC-4F78-86C7-098AE8C1D810}', 'Breakfast', '09:00:00'),
('{A40E2898-E506-4783-974E-F9E63D275F0B}', 'Dinner', '21:00:00'),
('{F950A12A-9290-4F22-97FE-B15B977BD699}', 'Lunch', '14:20:00');

-- --------------------------------------------------------

--
-- Table structure for table `mms_menu`
--

CREATE TABLE `mms_menu` (
  `id` varchar(40) NOT NULL,
  `mealType_id` varchar(40) NOT NULL,
  `housing_id` varchar(40) NOT NULL,
  `totalCost` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mms_menu`
--

INSERT INTO `mms_menu` (`id`, `mealType_id`, `housing_id`, `totalCost`) VALUES
('{06B03531-8BAA-4F38-BC22-958CDEB1F35C}', '{F950A12A-9290-4F22-97FE-B15B977BD699}', '2', 470),
('{10646EFC-301B-4596-858A-1BAC2DF3F351}', '{F950A12A-9290-4F22-97FE-B15B977BD699}', '1', 430),
('{2BF76DEA-D437-4037-A73F-AED5A4348BED}', '{A40E2898-E506-4783-974E-F9E63D275F0B}', '2', 130),
('{47E1EB6F-34F8-4BA6-9699-C0967485BB2A}', '{A40E2898-E506-4783-974E-F9E63D275F0B}', '1', 120),
('{7758C07D-4D1B-41E3-BAE0-642F932EA8CF}', '{2ADCE5CC-28EC-4F78-86C7-098AE8C1D810}', '2', 16),
('{8A566225-FC8B-4DB0-9DED-5D2AED0352AD}', '{2ADCE5CC-28EC-4F78-86C7-098AE8C1D810}', '1', 330),
('{C5621510-ADBD-4878-B5A3-AE8E654DEAEC}', '{2ADCE5CC-28EC-4F78-86C7-098AE8C1D810}', '1', 250);

-- --------------------------------------------------------

--
-- Table structure for table `mms_offer`
--

CREATE TABLE `mms_offer` (
  `id` varchar(40) NOT NULL,
  `housing_id` varchar(40) NOT NULL,
  `menu_id` varchar(40) NOT NULL,
  `offerDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mms_offer`
--

INSERT INTO `mms_offer` (`id`, `housing_id`, `menu_id`, `offerDate`) VALUES
('{14C1067A-8A17-4304-8AE4-50914E5242C5}', '2', '{7758C07D-4D1B-41E3-BAE0-642F932EA8CF}', '2017-10-28'),
('{77B3AF48-B675-4183-A54E-F0E7AECCFE08}', '1', '{10646EFC-301B-4596-858A-1BAC2DF3F351}', '2017-10-30'),
('{98685F8F-C30A-43A9-BE50-67EFEFD564C0}', '1', '{C5621510-ADBD-4878-B5A3-AE8E654DEAEC}', '2017-10-29'),
('{BF2C3D37-C5D2-42EF-B70D-96A02D19C952}', '2', '{06B03531-8BAA-4F38-BC22-958CDEB1F35C}', '2017-10-27'),
('{D7C2F7E5-D076-42FC-A5E5-1C8E85695997}', '1', '{8A566225-FC8B-4DB0-9DED-5D2AED0352AD}', '2017-10-30'),
('{E33A05C7-23FB-402C-ABE3-9157D5DD41E5}', '1', '{47E1EB6F-34F8-4BA6-9699-C0967485BB2A}', '2017-10-31');

-- --------------------------------------------------------

--
-- Table structure for table `mms_order`
--

CREATE TABLE `mms_order` (
  `id` varchar(40) NOT NULL,
  `menu_id` varchar(40) NOT NULL,
  `user_id` varchar(40) NOT NULL,
  `housing_id` varchar(40) NOT NULL,
  `orderDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mms_order`
--

INSERT INTO `mms_order` (`id`, `menu_id`, `user_id`, `housing_id`, `orderDate`) VALUES
('{451752CC-FE21-4580-929B-9DC85F892041}', '{06B03531-8BAA-4F38-BC22-958CDEB1F35C}', 'another@test.com', '2', '2017-10-26 10:19:27'),
('{4CED5F28-2E2B-448D-BA83-681F89FD4272}', '{7758C07D-4D1B-41E3-BAE0-642F932EA8CF}', 'another@test.com', '2', '2017-10-26 10:19:32'),
('{5ECA339D-96FC-4C29-BB2E-B10815BD7460}', '{C5621510-ADBD-4878-B5A3-AE8E654DEAEC}', 'test@test.com', '1', '2017-10-26 10:25:40'),
('{A205A814-9E7C-4963-8A8C-0EDECE4E7216}', '{06B03531-8BAA-4F38-BC22-958CDEB1F35C}', 'super@test.com', '2', '2017-10-26 10:21:16'),
('{B6B4E4AD-71C9-40C5-944B-2C02FF90AD72}', '{47E1EB6F-34F8-4BA6-9699-C0967485BB2A}', 'another@test.com', '1', '2017-10-26 10:21:45'),
('{CCD66FF3-A1E8-47BF-BA32-55933F942BA1}', '{10646EFC-301B-4596-858A-1BAC2DF3F351}', 'super@test.com', '1', '2017-10-26 10:18:50');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `ID` varchar(40) NOT NULL,
  `UniversityID` varchar(20) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Password` varchar(20) NOT NULL,
  `FirstName` varchar(50) NOT NULL,
  `LastName` varchar(50) NOT NULL,
  `Status` varchar(20) DEFAULT NULL,
  `IsLogged` smallint(1) DEFAULT NULL,
  `IsArchived` smallint(1) DEFAULT NULL,
  `IsDeleted` smallint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`ID`, `UniversityID`, `Email`, `Password`, `FirstName`, `LastName`, `Status`, `IsLogged`, `IsArchived`, `IsDeleted`) VALUES
('another1@test.com', '020205', 'another1@test.com', '123', 'Another1', 'Test ', 'approved', NULL, NULL, NULL),
('another@test.com', '020202', 'another@test.com', '123', 'Another', 'Test     ', 'approved', NULL, NULL, NULL),
('super@test.com', '020203', 'super@test.com', '123', 'Super', 'Test', 'approved', NULL, NULL, NULL),
('test1@test.com', '140201', 'test1@test.com', '123', 'test', 'test', 'approved', NULL, NULL, NULL),
('test@test.com', '020201', 'test@test.com', '123', 'I AM', 'ADMIN ', 'approved', NULL, NULL, NULL),
('working@test.com', '020204', 'working@test.com', '123', 'Working', 'Test', 'pending', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hms_hall`
--
ALTER TABLE `hms_hall`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mms_food`
--
ALTER TABLE `mms_food`
  ADD PRIMARY KEY (`id`),
  ADD KEY `housing_id` (`housing_id`);

--
-- Indexes for table `mms_food_menu`
--
ALTER TABLE `mms_food_menu`
  ADD KEY `food_id` (`food_id`),
  ADD KEY `menu_id` (`menu_id`);

--
-- Indexes for table `mms_housing`
--
ALTER TABLE `mms_housing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mms_mealtype`
--
ALTER TABLE `mms_mealtype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mms_menu`
--
ALTER TABLE `mms_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mealType_id` (`mealType_id`),
  ADD KEY `housing_id` (`housing_id`);

--
-- Indexes for table `mms_offer`
--
ALTER TABLE `mms_offer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_id` (`menu_id`),
  ADD KEY `housing_id` (`housing_id`);

--
-- Indexes for table `mms_order`
--
ALTER TABLE `mms_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_id` (`menu_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `housing_id` (`housing_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `Email` (`Email`),
  ADD UNIQUE KEY `UniversityID` (`UniversityID`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `mms_food`
--
ALTER TABLE `mms_food`
  ADD CONSTRAINT `mms_food_ibfk_1` FOREIGN KEY (`housing_id`) REFERENCES `mms_housing` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mms_food_menu`
--
ALTER TABLE `mms_food_menu`
  ADD CONSTRAINT `mms_food_menu_ibfk_1` FOREIGN KEY (`food_id`) REFERENCES `mms_food` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mms_food_menu_ibfk_2` FOREIGN KEY (`menu_id`) REFERENCES `mms_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mms_menu`
--
ALTER TABLE `mms_menu`
  ADD CONSTRAINT `mms_menu_ibfk_1` FOREIGN KEY (`housing_id`) REFERENCES `mms_housing` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mms_menu_ibfk_2` FOREIGN KEY (`mealType_id`) REFERENCES `mms_mealtype` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mms_offer`
--
ALTER TABLE `mms_offer`
  ADD CONSTRAINT `mms_offer_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `mms_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mms_offer_ibfk_2` FOREIGN KEY (`housing_id`) REFERENCES `mms_housing` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mms_order`
--
ALTER TABLE `mms_order`
  ADD CONSTRAINT `mms_order_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `mms_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mms_order_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mms_order_ibfk_3` FOREIGN KEY (`housing_id`) REFERENCES `mms_housing` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
