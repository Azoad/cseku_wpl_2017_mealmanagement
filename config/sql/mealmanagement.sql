-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 07, 2017 at 04:13 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mealmanagement`
--

-- --------------------------------------------------------

--
-- Table structure for table `hms_hall`
--

CREATE TABLE `hms_hall` (
  `id` varchar(40) NOT NULL,
  `name` varchar(40) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `provost` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mms_food`
--

CREATE TABLE `mms_food` (
  `id` varchar(40) NOT NULL,
  `foodName` varchar(256) NOT NULL,
  `foodCost` double NOT NULL,
  `housing_id` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mms_food`
--

INSERT INTO `mms_food` (`id`, `foodName`, `foodCost`, `housing_id`) VALUES
('{038B23C8-9CC2-462E-9845-DACE73F3655F}', 'Chicken Fry', 50, '1'),
('{08DDB25E-5F9C-4A1D-85CD-D8892A11303D}', 'Pizza', 350, '2'),
('{456B983D-2B94-4FCA-A4EB-A7A20C0F50AC}', 'Shawarma', 100, '2'),
('{4BD0FF53-78AB-433B-B711-B82C6A6CBB8F}', 'Raw Tea', 6, '2'),
('{98291889-48AF-410D-A144-A17DEDD2698C}', '7up', 20, '2'),
('{9A9438DA-95B1-48A3-AD05-60B2E6298C87}', 'Burger', 110, '2'),
('{AF27B395-E592-48E8-9CE0-A2F48EE81C35}', 'Berger', 80, '1'),
('{B3073EBC-C1A6-4510-89BD-48CB0B1C6694}', 'Fried Rice', 200, '1'),
('{DD7018D6-3A48-443E-A9F0-5E1912CF2EA9}', 'Coffee', 40, '1'),
('{E2A59F06-DD57-4C5B-B12D-FA8463FA21F2}', 'Bun', 10, '2'),
('{E7999A4C-8731-4D36-9590-0A839667ECE7}', 'Pizza', 100, '1');

-- --------------------------------------------------------

--
-- Table structure for table `mms_food_menu`
--

CREATE TABLE `mms_food_menu` (
  `food_id` varchar(40) NOT NULL,
  `menu_id` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mms_food_menu`
--

INSERT INTO `mms_food_menu` (`food_id`, `menu_id`) VALUES
('{038B23C8-9CC2-462E-9845-DACE73F3655F}', '{C5621510-ADBD-4878-B5A3-AE8E654DEAEC}'),
('{B3073EBC-C1A6-4510-89BD-48CB0B1C6694}', '{C5621510-ADBD-4878-B5A3-AE8E654DEAEC}'),
('{038B23C8-9CC2-462E-9845-DACE73F3655F}', '{10646EFC-301B-4596-858A-1BAC2DF3F351}'),
('{E7999A4C-8731-4D36-9590-0A839667ECE7}', '{10646EFC-301B-4596-858A-1BAC2DF3F351}'),
('{DD7018D6-3A48-443E-A9F0-5E1912CF2EA9}', '{47E1EB6F-34F8-4BA6-9699-C0967485BB2A}'),
('{AF27B395-E592-48E8-9CE0-A2F48EE81C35}', '{47E1EB6F-34F8-4BA6-9699-C0967485BB2A}'),
('{E2A59F06-DD57-4C5B-B12D-FA8463FA21F2}', '{7758C07D-4D1B-41E3-BAE0-642F932EA8CF}'),
('{4BD0FF53-78AB-433B-B711-B82C6A6CBB8F}', '{7758C07D-4D1B-41E3-BAE0-642F932EA8CF}'),
('{08DDB25E-5F9C-4A1D-85CD-D8892A11303D}', '{06B03531-8BAA-4F38-BC22-958CDEB1F35C}'),
('{98291889-48AF-410D-A144-A17DEDD2698C}', '{06B03531-8BAA-4F38-BC22-958CDEB1F35C}'),
('{456B983D-2B94-4FCA-A4EB-A7A20C0F50AC}', '{06B03531-8BAA-4F38-BC22-958CDEB1F35C}'),
('{9A9438DA-95B1-48A3-AD05-60B2E6298C87}', '{2BF76DEA-D437-4037-A73F-AED5A4348BED}'),
('{98291889-48AF-410D-A144-A17DEDD2698C}', '{2BF76DEA-D437-4037-A73F-AED5A4348BED}'),
('{AF27B395-E592-48E8-9CE0-A2F48EE81C35}', '{10646EFC-301B-4596-858A-1BAC2DF3F351}'),
('{B3073EBC-C1A6-4510-89BD-48CB0B1C6694}', '{10646EFC-301B-4596-858A-1BAC2DF3F351}'),
('{038B23C8-9CC2-462E-9845-DACE73F3655F}', '{8A566225-FC8B-4DB0-9DED-5D2AED0352AD}'),
('{AF27B395-E592-48E8-9CE0-A2F48EE81C35}', '{8A566225-FC8B-4DB0-9DED-5D2AED0352AD}'),
('{B3073EBC-C1A6-4510-89BD-48CB0B1C6694}', '{8A566225-FC8B-4DB0-9DED-5D2AED0352AD}');

-- --------------------------------------------------------

--
-- Table structure for table `mms_housing`
--

CREATE TABLE `mms_housing` (
  `id` varchar(40) NOT NULL,
  `housingName` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mms_housing`
--

INSERT INTO `mms_housing` (`id`, `housingName`) VALUES
('1', 'Khan Jahan Ali Hall'),
('2', 'Khan Bahadur Ahsanullah Hall');

-- --------------------------------------------------------

--
-- Table structure for table `mms_mealtype`
--

CREATE TABLE `mms_mealtype` (
  `id` varchar(40) NOT NULL,
  `mealTypeName` varchar(256) NOT NULL,
  `time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mms_mealtype`
--

INSERT INTO `mms_mealtype` (`id`, `mealTypeName`, `time`) VALUES
('{2ADCE5CC-28EC-4F78-86C7-098AE8C1D810}', 'Breakfast', '09:00:00'),
('{A40E2898-E506-4783-974E-F9E63D275F0B}', 'Dinner', '21:00:00'),
('{F950A12A-9290-4F22-97FE-B15B977BD699}', 'Lunch', '14:20:00');

-- --------------------------------------------------------

--
-- Table structure for table `mms_menu`
--

CREATE TABLE `mms_menu` (
  `id` varchar(40) NOT NULL,
  `mealType_id` varchar(40) NOT NULL,
  `housing_id` varchar(40) NOT NULL,
  `totalCost` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mms_menu`
--

INSERT INTO `mms_menu` (`id`, `mealType_id`, `housing_id`, `totalCost`) VALUES
('{06B03531-8BAA-4F38-BC22-958CDEB1F35C}', '{F950A12A-9290-4F22-97FE-B15B977BD699}', '2', 470),
('{10646EFC-301B-4596-858A-1BAC2DF3F351}', '{F950A12A-9290-4F22-97FE-B15B977BD699}', '1', 430),
('{2BF76DEA-D437-4037-A73F-AED5A4348BED}', '{A40E2898-E506-4783-974E-F9E63D275F0B}', '2', 130),
('{47E1EB6F-34F8-4BA6-9699-C0967485BB2A}', '{A40E2898-E506-4783-974E-F9E63D275F0B}', '1', 120),
('{7758C07D-4D1B-41E3-BAE0-642F932EA8CF}', '{2ADCE5CC-28EC-4F78-86C7-098AE8C1D810}', '2', 16),
('{8A566225-FC8B-4DB0-9DED-5D2AED0352AD}', '{2ADCE5CC-28EC-4F78-86C7-098AE8C1D810}', '1', 330),
('{C5621510-ADBD-4878-B5A3-AE8E654DEAEC}', '{2ADCE5CC-28EC-4F78-86C7-098AE8C1D810}', '1', 250);

-- --------------------------------------------------------

--
-- Table structure for table `mms_offer`
--

CREATE TABLE `mms_offer` (
  `id` varchar(40) NOT NULL,
  `housing_id` varchar(40) NOT NULL,
  `menu_id` varchar(40) NOT NULL,
  `offerDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mms_offer`
--

INSERT INTO `mms_offer` (`id`, `housing_id`, `menu_id`, `offerDate`) VALUES
('{14C1067A-8A17-4304-8AE4-50914E5242C5}', '2', '{7758C07D-4D1B-41E3-BAE0-642F932EA8CF}', '2017-10-28'),
('{77B3AF48-B675-4183-A54E-F0E7AECCFE08}', '1', '{10646EFC-301B-4596-858A-1BAC2DF3F351}', '2017-10-30'),
('{98685F8F-C30A-43A9-BE50-67EFEFD564C0}', '1', '{C5621510-ADBD-4878-B5A3-AE8E654DEAEC}', '2017-10-29'),
('{BF2C3D37-C5D2-42EF-B70D-96A02D19C952}', '2', '{06B03531-8BAA-4F38-BC22-958CDEB1F35C}', '2017-10-27'),
('{D7C2F7E5-D076-42FC-A5E5-1C8E85695997}', '1', '{8A566225-FC8B-4DB0-9DED-5D2AED0352AD}', '2017-10-30'),
('{E33A05C7-23FB-402C-ABE3-9157D5DD41E5}', '1', '{47E1EB6F-34F8-4BA6-9699-C0967485BB2A}', '2017-10-31');

-- --------------------------------------------------------

--
-- Table structure for table `mms_order`
--

CREATE TABLE `mms_order` (
  `id` varchar(40) NOT NULL,
  `menu_id` varchar(40) NOT NULL,
  `user_id` varchar(40) NOT NULL,
  `housing_id` varchar(40) NOT NULL,
  `orderDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mms_order`
--

INSERT INTO `mms_order` (`id`, `menu_id`, `user_id`, `housing_id`, `orderDate`) VALUES
('{451752CC-FE21-4580-929B-9DC85F892041}', '{06B03531-8BAA-4F38-BC22-958CDEB1F35C}', 'another@test.com', '2', '2017-10-26 10:19:27'),
('{4CED5F28-2E2B-448D-BA83-681F89FD4272}', '{7758C07D-4D1B-41E3-BAE0-642F932EA8CF}', 'another@test.com', '2', '2017-10-26 10:19:32'),
('{5ECA339D-96FC-4C29-BB2E-B10815BD7460}', '{C5621510-ADBD-4878-B5A3-AE8E654DEAEC}', 'test@test.com', '1', '2017-10-26 10:25:40'),
('{A205A814-9E7C-4963-8A8C-0EDECE4E7216}', '{06B03531-8BAA-4F38-BC22-958CDEB1F35C}', 'super@test.com', '2', '2017-10-26 10:21:16'),
('{B6B4E4AD-71C9-40C5-944B-2C02FF90AD72}', '{47E1EB6F-34F8-4BA6-9699-C0967485BB2A}', 'another@test.com', '1', '2017-10-26 10:21:45'),
('{CCD66FF3-A1E8-47BF-BA32-55933F942BA1}', '{10646EFC-301B-4596-858A-1BAC2DF3F351}', 'super@test.com', '1', '2017-10-26 10:18:50');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_discipline`
--

CREATE TABLE `tbl_discipline` (
  `ID` varchar(40) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `ShortCode` varchar(20) DEFAULT NULL,
  `SchoolID` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_discipline`
--

INSERT INTO `tbl_discipline` (`ID`, `Name`, `ShortCode`, `SchoolID`) VALUES
('{0CF37516-06FE-41CD-93AD-D2D1652509D6}', 'Mathematics', 'MATH', '{39DDC0C2-CFC2-4246-8748-8812B1751A5C}'),
('{560A0FC0-6221-497D-8D41-E584EE4BBEE3}', 'Architecture', 'ARCH', '{39DDC0C2-CFC2-4246-8748-8812B1751A5C}'),
('{63F3C00B-6168-4FBD-AB01-7A1FE413BDDE}', 'Forestry and Wood Technology', 'FWT', '{4D46079B-AFA3-40F1-B8D1-6CC9E9F56812}'),
('{AF41CC9F-3BCD-4952-9D02-17184CC40C79}', 'Urban and Rural Planning', 'URP', '{4D46079B-AFA3-40F1-B8D1-6CC9E9F56812}'),
('{E065BBA7-D0C5-4DFA-9768-81B6F056EB4A}', 'Foresty and Marine Resource Technology', 'FMRT', '{4D46079B-AFA3-40F1-B8D1-6CC9E9F56812}'),
('{E7280448-E379-424E-A11D-357F4334AC8D}', 'Physics', 'PHY', '{39DDC0C2-CFC2-4246-8748-8812B1751A5C}'),
('{FFDB1CB8-AF34-4381-8971-9784DCB516C5}', 'Computer Science and Engineering', 'CSE', '{39DDC0C2-CFC2-4246-8748-8812B1751A5C}');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_discussion`
--

CREATE TABLE `tbl_discussion` (
  `ID` varchar(40) NOT NULL,
  `Title` varchar(150) NOT NULL,
  `CategoryID` varchar(40) NOT NULL,
  `Description` varchar(300) NOT NULL,
  `Tag` varchar(200) NOT NULL,
  `CreationDate` datetime NOT NULL,
  `CreatorID` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_discussion`
--

INSERT INTO `tbl_discussion` (`ID`, `Title`, `CategoryID`, `Description`, `Tag`, `CreationDate`, `CreatorID`) VALUES
('{C9FB74F8-8341-4706-BE40-93BFDC3444D0}', 'what is oop', '{3D212234-2F34-4AB0-83EF-1A772068403B}', 'describe oop', 'oop', '2017-04-29 00:00:00', 'mkazi078@uottawa.ca'),
('{DA408BD0-9C9E-46F6-8CF2-00A631B06A26}', 'what is c#', '{44CAEE8D-A9D7-48C8-A2EA-A7463E00FCD6}', 'this is c#', 'oop', '2017-04-29 00:00:00', 'mkazi078@uottawa.ca');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_discussion_category`
--

CREATE TABLE `tbl_discussion_category` (
  `ID` varchar(40) NOT NULL,
  `Name` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_discussion_category`
--

INSERT INTO `tbl_discussion_category` (`ID`, `Name`) VALUES
('{3D212234-2F34-4AB0-83EF-1A772068403B}', 'java'),
('{44CAEE8D-A9D7-48C8-A2EA-A7463E00FCD6}', 'c#'),
('{974DE90C-BCAC-4FA3-8B9B-518A3CE6834A}', 'programming contest');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_discussion_comment`
--

CREATE TABLE `tbl_discussion_comment` (
  `CommentID` varchar(50) NOT NULL,
  `DiscussionID` varchar(40) NOT NULL,
  `Comment` varchar(300) NOT NULL,
  `CreatorID` varchar(40) NOT NULL,
  `CommentTime` datetime NOT NULL,
  `CommentIDTop` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_discussion_comment`
--

INSERT INTO `tbl_discussion_comment` (`CommentID`, `DiscussionID`, `Comment`, `CreatorID`, `CommentTime`, `CommentIDTop`) VALUES
('{00AADED4-6799-4F2C-BECB-ED50F7B03DDE}', '{C9FB74F8-8341-4706-BE40-93BFDC3444D0}', 'new comment', 'mkazi078@uottawa.ca', '2017-06-26 19:18:08', NULL),
('{1634B01B-5F82-43EF-96F8-E6149F488424}', '{C9FB74F8-8341-4706-BE40-93BFDC3444D0}', 'it is PIE', 'mkazi078@uottawa.ca', '0000-00-00 00:00:00', NULL),
('{550A15FC-06B8-43DF-83EE-097E35920170}', '{C9FB74F8-8341-4706-BE40-93BFDC3444D0}', 'little difficult', 'mohidul@gmail.com', '0000-00-00 00:00:00', NULL),
('{A15517C2-883F-4E5E-B0AC-9A1DB556741F}', '{C9FB74F8-8341-4706-BE40-93BFDC3444D0}', 'Polymorphism, inheritence, encapsulation', 'mkazi078@uottawa.ca', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_permission`
--

CREATE TABLE `tbl_permission` (
  `ID` varchar(100) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Category` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_permission`
--

INSERT INTO `tbl_permission` (`ID`, `Name`, `Category`) VALUES
('DISCIPLINE_C', 'DISCIPLINE_C', 'DISCIPLINE'),
('DISCIPLINE_D', 'DISCIPLINE_D', 'DISCIPLINE'),
('DISCIPLINE_R', 'DISCIPLINE_R', 'DISCIPLINE'),
('DISCIPLINE_U', 'DISCIPLINE_U', 'DISCIPLINE'),
('DISCUSSION_C', 'DISCUSSION_C', 'DISCUSSION'),
('DISCUSSION_CAT_C', 'DISCUSSION_CAT_C', 'DISCUSSION CATEGORY'),
('DISCUSSION_CAT_D', 'DISCUSSION_CAT_D', 'DISCUSSION CATEGORY'),
('DISCUSSION_CAT_R', 'DISCUSSION_CAT_R', 'DISCUSSION CATEGORY'),
('DISCUSSION_CAT_U', 'DISCUSSION_CAT_U', 'DISCUSSION CATEGORY'),
('DISCUSSION_COMMENT_C', 'DISCUSSION_COMMENT_C', 'DISCUSSION COMMENT'),
('DISCUSSION_COMMENT_D', 'DISCUSSION_COMMENT_D', 'DISCUSSION COMMENT'),
('DISCUSSION_COMMENT_R', 'DISCUSSION_COMMENT_R', 'DISCUSSION COMMENT'),
('DISCUSSION_COMMENT_U', 'DISCUSSION_COMMENT_U', 'DISCUSSION COMMENT'),
('DISCUSSION_D', 'DISCUSSION_D', 'DISCUSSION'),
('DISCUSSION_R', 'DISCUSSION_R', 'DISCUSSION'),
('DISCUSSION_U', 'DISCUSSION_U', 'DISCUSSION'),
('FOOD_C', 'FOOD_C', 'FOOD'),
('FOOD_D', 'FOOD_D', 'FOOD'),
('FOOD_MENU_C', 'FOOD_MENU_C', 'FOOD_MENU'),
('FOOD_MENU_D', 'FOOD_MENU_D', 'FOOD_MENU'),
('FOOD_MENU_R', 'FOOD_MENU_R', 'FOOD_MENU'),
('FOOD_MENU_U', 'FOOD_MENU_U', 'FOOD_MENU'),
('FOOD_R', 'FOOD_R', 'FOOD'),
('FOOD_U', 'FOOD_U', 'FOOD'),
('HALL_C', 'HALL_C', 'HALL'),
('HALL_D', 'HALL_D', 'HALL'),
('HALL_R', 'HALL_R', 'HALL'),
('HALL_U', 'HALL_U', 'HALL'),
('HOUSING_C', 'HOUSING_C', 'HOUSING'),
('HOUSING_D', 'HOUSING_D', 'HOUSING'),
('HOUSING_R', 'HOUSING_R', 'HOUSING'),
('HOUSING_U', 'HOUSING_U', 'HOUSING'),
('MEALTYPE_C', 'MEALTYPE_C', 'MEALTYPE'),
('MEALTYPE_D', 'MEALTYPE_D', 'MEALTYPE'),
('MEALTYPE_R', 'MEALTYPE_R', 'MEALTYPE'),
('MEALTYPE_U', 'MEALTYPE_U', 'MEALTYPE'),
('MENU_C', 'MENU_C', 'MENU'),
('MENU_D', 'MENU_D', 'MENU'),
('MENU_R', 'MENU_R', 'MENU'),
('MENU_U', 'MENU_U', 'MENU'),
('OFFER_C', 'OFFER_C', 'OFFER'),
('OFFER_D', 'OFFER_D', 'OFFER'),
('OFFER_R', 'OFFER_R', 'OFFER'),
('OFFER_U', 'OFFER_U', 'OFFER'),
('ORDER_C', 'ORDER_C', 'ORDER'),
('ORDER_D', 'ORDER_D', 'ORDER'),
('ORDER_R', 'ORDER_R', 'ORDER'),
('ORDER_U', 'ORDER_U', 'ORDER'),
('PERMISSION_C', 'PERMISSION_C', 'PERMISSION'),
('PERMISSION_D', 'PERMISSION_D', 'PERMISSION'),
('PERMISSION_R', 'PERMISSION_R', 'PERMISSION'),
('PERMISSION_U', 'PERMISSION_U', 'PERMISSION'),
('POSITION_C', 'POSITION_C', 'POSITION'),
('POSITION_D', 'POSITION_D', 'POSITION'),
('POSITION_R', 'POSITION_R', 'POSITION'),
('POSITION_U', 'POSITION_U', 'POSITION'),
('ROLE_C', 'ROLE_C', 'ROLE'),
('ROLE_D', 'ROLE_D', 'ROLE'),
('ROLE_R', 'ROLE_R', 'ROLE'),
('ROLE_U', 'ROLE_U', 'ROLE'),
('SCHOOL_C', 'SCHOOL_C', 'SCHOOL'),
('SCHOOL_D', 'SCHOOL_D', 'SCHOOL'),
('SCHOOL_R', 'SCHOOL_R', 'SCHOOL'),
('SCHOOL_U', 'SCHOOL_U', 'SCHOOL'),
('TERM_C', 'TERM_C', 'TERM'),
('TERM_D', 'TERM_D', 'TERM'),
('TERM_R', 'TERM_R', 'TERM'),
('TERM_U', 'TERM_U', 'TERM'),
('USER_C', 'USER_C', 'USER'),
('USER_D', 'USER_D', 'USER'),
('USER_R', 'USER_R', 'USER'),
('USER_U', 'USER_U', 'USER'),
('YEAR_C', 'YEAR_C', 'YEAR'),
('YEAR_D', 'YEAR_D', 'YEAR'),
('YEAR_R', 'YEAR_R', 'YEAR'),
('YEAR_U', 'YEAR_U', 'YEAR');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_position`
--

CREATE TABLE `tbl_position` (
  `ID` varchar(40) NOT NULL,
  `Name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_position`
--

INSERT INTO `tbl_position` (`ID`, `Name`) VALUES
('{1BFE76DB-C2AA-4FAA-A23B-F43E6150A2F6}', 'Section Officer'),
('{2E024DF5-BD45-4EF2-A5E3-43BCA3E9777F}', 'Pro-vice Chancellor'),
('{64D25DDA-16B6-47B8-BBFC-4E2AAF5680C7}', 'Assistant Professor'),
('{7CDA1F32-A2F8-4469-B5A8-C2038FCE1F9A}', 'Lecturer'),
('{818DE12F-60CC-42E4-BAEC-48EAAED65179}', 'Dean of School'),
('{928EE9FF-E02D-470F-9A6A-AD0EB38B848F}', 'Vice Chancellor'),
('{92FDDA3F-1E91-4AA3-918F-EB595F85790C}', 'Daywise Worker'),
('{932CB0EE-76C2-448E-A40E-2D167EECC479}', 'Registrar'),
('{ADA027D3-21C1-47AF-A21D-759CAFCFE58D}', 'Assistant Registrar'),
('{B76EB035-EA26-4CEB-B029-1C6129574D98}', 'Librarian'),
('{B78C7A7B-4D38-4025-8170-7B8C9F291946}', 'Head of Discipline'),
('{C27B6BCF-FB83-4F3D-85CA-B7870D8B12D0}', 'Associate Professor'),
('{EB4880E2-01B3-4C6E-A2C9-89B6E5427C0A}', 'Professor');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_role`
--

CREATE TABLE `tbl_role` (
  `ID` varchar(40) NOT NULL,
  `Name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_role`
--

INSERT INTO `tbl_role` (`ID`, `Name`) VALUES
('administrator', 'Administrator'),
('hall assign', 'Hall Assign'),
('registration coordinator', 'Registration Coordinator'),
('student', 'Student'),
('stuff', 'Stuff'),
('tabulator', 'Tabulator'),
('teacher', 'Teacher');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_role_permission`
--

CREATE TABLE `tbl_role_permission` (
  `Row` int(11) NOT NULL,
  `RoleID` varchar(40) NOT NULL,
  `PermissionID` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_role_permission`
--

INSERT INTO `tbl_role_permission` (`Row`, `RoleID`, `PermissionID`) VALUES
(1809, 'hall assign', 'TERM_C'),
(1810, 'hall assign', 'TERM_D'),
(1811, 'hall assign', 'TERM_R'),
(1812, 'hall assign', 'TERM_U'),
(1813, 'hall assign', 'USER_C'),
(1814, 'hall assign', 'USER_D'),
(1815, 'hall assign', 'USER_R'),
(1816, 'hall assign', 'USER_U'),
(1817, 'hall assign', 'YEAR_C'),
(1818, 'hall assign', 'YEAR_D'),
(1819, 'hall assign', 'YEAR_R'),
(1820, 'hall assign', 'YEAR_U'),
(1893, 'administrator', 'DISCIPLINE_C'),
(1894, 'administrator', 'DISCIPLINE_D'),
(1895, 'administrator', 'DISCIPLINE_R'),
(1896, 'administrator', 'DISCIPLINE_U'),
(1897, 'administrator', 'DISCUSSION_C'),
(1898, 'administrator', 'DISCUSSION_D'),
(1899, 'administrator', 'DISCUSSION_R'),
(1900, 'administrator', 'DISCUSSION_U'),
(1901, 'administrator', 'DISCUSSION_CAT_C'),
(1902, 'administrator', 'DISCUSSION_CAT_D'),
(1903, 'administrator', 'DISCUSSION_CAT_R'),
(1904, 'administrator', 'DISCUSSION_CAT_U'),
(1905, 'administrator', 'DISCUSSION_COMMENT_C'),
(1906, 'administrator', 'DISCUSSION_COMMENT_D'),
(1907, 'administrator', 'DISCUSSION_COMMENT_R'),
(1908, 'administrator', 'DISCUSSION_COMMENT_U'),
(1909, 'administrator', 'FOOD_C'),
(1910, 'administrator', 'FOOD_D'),
(1911, 'administrator', 'FOOD_R'),
(1912, 'administrator', 'FOOD_U'),
(1913, 'administrator', 'FOOD_MENU_R'),
(1914, 'administrator', 'FOOD_MENU_U'),
(1915, 'administrator', 'FOOD_MENU_C'),
(1916, 'administrator', 'FOOD_MENU_D'),
(1917, 'administrator', 'HOUSING_C'),
(1918, 'administrator', 'HOUSING_D'),
(1919, 'administrator', 'HOUSING_R'),
(1920, 'administrator', 'HOUSING_U'),
(1921, 'administrator', 'MEALTYPE_C'),
(1922, 'administrator', 'MEALTYPE_D'),
(1923, 'administrator', 'MEALTYPE_R'),
(1924, 'administrator', 'MEALTYPE_U'),
(1925, 'administrator', 'MENU_C'),
(1926, 'administrator', 'MENU_D'),
(1927, 'administrator', 'MENU_R'),
(1928, 'administrator', 'MENU_U'),
(1929, 'administrator', 'OFFER_C'),
(1930, 'administrator', 'OFFER_D'),
(1931, 'administrator', 'OFFER_R'),
(1932, 'administrator', 'OFFER_U'),
(1933, 'administrator', 'ORDER_C'),
(1934, 'administrator', 'ORDER_D'),
(1935, 'administrator', 'ORDER_R'),
(1936, 'administrator', 'ORDER_U'),
(1937, 'administrator', 'PERMISSION_C'),
(1938, 'administrator', 'PERMISSION_D'),
(1939, 'administrator', 'PERMISSION_R'),
(1940, 'administrator', 'PERMISSION_U'),
(1941, 'administrator', 'POSITION_C'),
(1942, 'administrator', 'POSITION_D'),
(1943, 'administrator', 'POSITION_R'),
(1944, 'administrator', 'POSITION_U'),
(1945, 'administrator', 'ROLE_C'),
(1946, 'administrator', 'ROLE_D'),
(1947, 'administrator', 'ROLE_R'),
(1948, 'administrator', 'ROLE_U'),
(1949, 'administrator', 'SCHOOL_C'),
(1950, 'administrator', 'SCHOOL_D'),
(1951, 'administrator', 'SCHOOL_R'),
(1952, 'administrator', 'SCHOOL_U'),
(1953, 'administrator', 'TERM_C'),
(1954, 'administrator', 'TERM_D'),
(1955, 'administrator', 'TERM_R'),
(1956, 'administrator', 'TERM_U'),
(1957, 'administrator', 'USER_C'),
(1958, 'administrator', 'USER_D'),
(1959, 'administrator', 'USER_R'),
(1960, 'administrator', 'USER_U'),
(1961, 'administrator', 'YEAR_C'),
(1962, 'administrator', 'YEAR_D'),
(1963, 'administrator', 'YEAR_R'),
(1964, 'administrator', 'YEAR_U'),
(2427, 'student', 'FOOD_C'),
(2428, 'student', 'FOOD_D'),
(2429, 'student', 'FOOD_R'),
(2430, 'student', 'FOOD_U');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_school`
--

CREATE TABLE `tbl_school` (
  `ID` varchar(40) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `DeanID` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_school`
--

INSERT INTO `tbl_school` (`ID`, `Name`, `DeanID`) VALUES
('{39DDC0C2-CFC2-4246-8748-8812B1751A5C}', 'Science Engineering and Technology', ''),
('{4D46079B-AFA3-40F1-B8D1-6CC9E9F56812}', 'Life Science', ''),
('{86E0F021-B30D-48D2-B177-247180633C5E}', 'Social Science', ''),
('{879375F7-0A15-4705-AC90-C6786D279EF1}', 'Law and Humanities', ''),
('{CE09AA38-205B-4F50-A0E0-14DB8686C912}', 'Fine Arts', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_term`
--

CREATE TABLE `tbl_term` (
  `ID` varchar(40) NOT NULL,
  `Name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_term`
--

INSERT INTO `tbl_term` (`ID`, `Name`) VALUES
('{6DE3CF58-3A1A-4D6A-88CF-83F22C27E0BA}', 'Special'),
('{AF8B217E-4E76-41B8-A02A-7115BD4A6BE6}', '2nd'),
('{F9121C67-1E89-4F0B-80AA-89FD3B6BD665}', '1st');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `ID` varchar(40) NOT NULL,
  `UniversityID` varchar(20) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Password` varchar(20) NOT NULL,
  `FirstName` varchar(50) NOT NULL,
  `LastName` varchar(50) NOT NULL,
  `Status` varchar(20) DEFAULT NULL,
  `IsLogged` smallint(1) DEFAULT NULL,
  `IsArchived` smallint(1) DEFAULT NULL,
  `IsDeleted` smallint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`ID`, `UniversityID`, `Email`, `Password`, `FirstName`, `LastName`, `Status`, `IsLogged`, `IsArchived`, `IsDeleted`) VALUES
('another1@test.com', '020205', 'another1@test.com', '123', 'Another1', 'Test ', 'approved', NULL, NULL, NULL),
('another@test.com', '020202', 'another@test.com', '123', 'Another', 'Test     ', 'approved', NULL, NULL, NULL),
('super@test.com', '020203', 'super@test.com', '123', 'Super', 'Test', 'approved', NULL, NULL, NULL),
('test1@test.com', '140201', 'test1@test.com', '123', 'test', 'test', 'approved', NULL, NULL, NULL),
('test@test.com', '020201', 'test@test.com', '123', 'I AM', 'ADMIN ', 'approved', NULL, NULL, NULL),
('working@test.com', '020204', 'working@test.com', '123', 'Working', 'Test', 'pending', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_details`
--

CREATE TABLE `tbl_user_details` (
  `ID` varchar(40) NOT NULL,
  `FatherName` varchar(100) DEFAULT NULL,
  `MotherName` varchar(100) DEFAULT NULL,
  `PermanentAddress` varchar(200) DEFAULT NULL,
  `HomePhone` varchar(20) DEFAULT NULL,
  `CurrentAddress` varchar(200) DEFAULT NULL,
  `MobilePhone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_details`
--

INSERT INTO `tbl_user_details` (`ID`, `FatherName`, `MotherName`, `PermanentAddress`, `HomePhone`, `CurrentAddress`, `MobilePhone`) VALUES
('another1@test.com', NULL, NULL, NULL, NULL, NULL, NULL),
('another@test.com', NULL, NULL, NULL, NULL, NULL, NULL),
('super@test.com', NULL, NULL, NULL, NULL, NULL, NULL),
('test1@test.com', NULL, NULL, NULL, NULL, NULL, NULL),
('test@test.com', 'My father', 'My mother', 'My address', '04100000', 'Same', '0171100000'),
('working@test.com', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_discipline`
--

CREATE TABLE `tbl_user_discipline` (
  `UserID` varchar(40) NOT NULL,
  `DisciplineID` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_position`
--

CREATE TABLE `tbl_user_position` (
  `ID` int(11) NOT NULL,
  `UserID` varchar(40) NOT NULL,
  `PositionID` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_position`
--

INSERT INTO `tbl_user_position` (`ID`, `UserID`, `PositionID`) VALUES
(4, '{693F944F-328D-433A-9F94-459D92841645}', '{EB4880E2-01B3-4C6E-A2C9-89B6E5427C0A}'),
(14, '{E0F0AE1A-AECF-46C1-A148-4485036F3CCF}', '{EB4880E2-01B3-4C6E-A2C9-89B6E5427C0A}'),
(16, '{A4F96981-F014-46F6-BB93-87500C3CBB03}', '{7CDA1F32-A2F8-4469-B5A8-C2038FCE1F9A}'),
(17, '{0B2F4F89-2048-4504-AB17-0412CC624A05}', '{64D25DDA-16B6-47B8-BBFC-4E2AAF5680C7}'),
(19, '{8104FB4F-8E63-489D-8D90-DB45A9A2327B}', '{64D25DDA-16B6-47B8-BBFC-4E2AAF5680C7}'),
(21, '{8B24B76D-9969-4F71-ABC4-6EE59355C686}', '{64D25DDA-16B6-47B8-BBFC-4E2AAF5680C7}'),
(24, '{9E2E6363-A0FF-4C0F-B58F-D162725FB702}', '{C27B6BCF-FB83-4F3D-85CA-B7870D8B12D0}'),
(30, 'mohidul@gmail.com', '{64D25DDA-16B6-47B8-BBFC-4E2AAF5680C7}'),
(37, 'mkazi078@uottawa.ca', '{64D25DDA-16B6-47B8-BBFC-4E2AAF5680C7}'),
(41, 'test@test.com', '{64D25DDA-16B6-47B8-BBFC-4E2AAF5680C7}'),
(44, 'another@test.com', '{64D25DDA-16B6-47B8-BBFC-4E2AAF5680C7}'),
(45, 'another@test.com', '{ADA027D3-21C1-47AF-A21D-759CAFCFE58D}'),
(46, 'another@test.com', '{C27B6BCF-FB83-4F3D-85CA-B7870D8B12D0}'),
(47, 'another@test.com', '{92FDDA3F-1E91-4AA3-918F-EB595F85790C}'),
(48, 'another@test.com', '{818DE12F-60CC-42E4-BAEC-48EAAED65179}'),
(49, 'another@test.com', '{B78C7A7B-4D38-4025-8170-7B8C9F291946}'),
(50, 'another@test.com', '{7CDA1F32-A2F8-4469-B5A8-C2038FCE1F9A}'),
(51, 'another@test.com', '{B76EB035-EA26-4CEB-B029-1C6129574D98}'),
(52, 'another@test.com', '{2E024DF5-BD45-4EF2-A5E3-43BCA3E9777F}'),
(53, 'another@test.com', '{EB4880E2-01B3-4C6E-A2C9-89B6E5427C0A}'),
(54, 'another@test.com', '{932CB0EE-76C2-448E-A40E-2D167EECC479}'),
(55, 'another@test.com', '{1BFE76DB-C2AA-4FAA-A23B-F43E6150A2F6}'),
(56, 'another@test.com', '{928EE9FF-E02D-470F-9A6A-AD0EB38B848F}'),
(57, 'another1@test.com', '{1BFE76DB-C2AA-4FAA-A23B-F43E6150A2F6}');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_role`
--

CREATE TABLE `tbl_user_role` (
  `ID` int(11) NOT NULL,
  `UserID` varchar(40) NOT NULL,
  `RoleID` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_role`
--

INSERT INTO `tbl_user_role` (`ID`, `UserID`, `RoleID`) VALUES
(105, 'test@test.com', 'administrator'),
(106, 'test@test.com', 'teacher'),
(109, 'another@test.com', 'student'),
(110, 'super@test.com', 'student'),
(111, 'working@test.com', 'student'),
(113, 'another1@test.com', 'hall assign'),
(114, 'test1@test.com', 'hall assign');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_year`
--

CREATE TABLE `tbl_year` (
  `ID` varchar(40) NOT NULL,
  `Name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_year`
--

INSERT INTO `tbl_year` (`ID`, `Name`) VALUES
('{1FAC0F1A-9D60-43F6-AB07-C933D5A4AA30}', 'Phd 1st'),
('{326B168F-58CC-42F3-B71A-DFE8DBBC05E8}', 'MSc 1st'),
('{6780C884-E112-4F58-9503-E2110B615547}', '4th'),
('{9F3A6CBC-0115-4EC2-ABB3-8CCA431F6C2B}', '1st'),
('{A21965E4-4FE4-43AC-A77F-DABAC9B356F2}', '3rd'),
('{ADBEDD3E-D8EA-47AA-A068-C4C703DB4AE6}', 'MSc 2nd'),
('{B9D6CC05-7AD4-4666-80AB-80797A872743}', 'Phd 2nd'),
('{CBE08035-94CD-4610-B4E2-A0E844184056}', 'Phd 4th'),
('{E3823AA6-6BE5-4A07-93EA-FA59DE4F616F}', 'Phd 3rd'),
('{EA335D18-A1A8-4D15-9C7B-4A4700AD4543}', '2nd');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hms_hall`
--
ALTER TABLE `hms_hall`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mms_food`
--
ALTER TABLE `mms_food`
  ADD PRIMARY KEY (`id`),
  ADD KEY `housing_id` (`housing_id`);

--
-- Indexes for table `mms_food_menu`
--
ALTER TABLE `mms_food_menu`
  ADD KEY `food_id` (`food_id`),
  ADD KEY `menu_id` (`menu_id`);

--
-- Indexes for table `mms_housing`
--
ALTER TABLE `mms_housing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mms_mealtype`
--
ALTER TABLE `mms_mealtype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mms_menu`
--
ALTER TABLE `mms_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mealType_id` (`mealType_id`),
  ADD KEY `housing_id` (`housing_id`);

--
-- Indexes for table `mms_offer`
--
ALTER TABLE `mms_offer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_id` (`menu_id`),
  ADD KEY `housing_id` (`housing_id`);

--
-- Indexes for table `mms_order`
--
ALTER TABLE `mms_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_id` (`menu_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `housing_id` (`housing_id`);

--
-- Indexes for table `tbl_discipline`
--
ALTER TABLE `tbl_discipline`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_discussion`
--
ALTER TABLE `tbl_discussion`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_discussion_category`
--
ALTER TABLE `tbl_discussion_category`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_discussion_comment`
--
ALTER TABLE `tbl_discussion_comment`
  ADD PRIMARY KEY (`CommentID`);

--
-- Indexes for table `tbl_permission`
--
ALTER TABLE `tbl_permission`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_position`
--
ALTER TABLE `tbl_position`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_role`
--
ALTER TABLE `tbl_role`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_role_permission`
--
ALTER TABLE `tbl_role_permission`
  ADD PRIMARY KEY (`Row`);

--
-- Indexes for table `tbl_term`
--
ALTER TABLE `tbl_term`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `Email` (`Email`),
  ADD UNIQUE KEY `UniversityID` (`UniversityID`);

--
-- Indexes for table `tbl_user_details`
--
ALTER TABLE `tbl_user_details`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_user_position`
--
ALTER TABLE `tbl_user_position`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_user_role`
--
ALTER TABLE `tbl_user_role`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_year`
--
ALTER TABLE `tbl_year`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_role_permission`
--
ALTER TABLE `tbl_role_permission`
  MODIFY `Row` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2431;
--
-- AUTO_INCREMENT for table `tbl_user_position`
--
ALTER TABLE `tbl_user_position`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `tbl_user_role`
--
ALTER TABLE `tbl_user_role`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `mms_food`
--
ALTER TABLE `mms_food`
  ADD CONSTRAINT `mms_food_ibfk_1` FOREIGN KEY (`housing_id`) REFERENCES `mms_housing` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mms_food_menu`
--
ALTER TABLE `mms_food_menu`
  ADD CONSTRAINT `mms_food_menu_ibfk_1` FOREIGN KEY (`food_id`) REFERENCES `mms_food` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mms_food_menu_ibfk_2` FOREIGN KEY (`menu_id`) REFERENCES `mms_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mms_menu`
--
ALTER TABLE `mms_menu`
  ADD CONSTRAINT `mms_menu_ibfk_1` FOREIGN KEY (`housing_id`) REFERENCES `mms_housing` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mms_menu_ibfk_2` FOREIGN KEY (`mealType_id`) REFERENCES `mms_mealtype` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mms_offer`
--
ALTER TABLE `mms_offer`
  ADD CONSTRAINT `mms_offer_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `mms_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mms_offer_ibfk_2` FOREIGN KEY (`housing_id`) REFERENCES `mms_housing` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mms_order`
--
ALTER TABLE `mms_order`
  ADD CONSTRAINT `mms_order_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `mms_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mms_order_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mms_order_ibfk_3` FOREIGN KEY (`housing_id`) REFERENCES `mms_housing` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
