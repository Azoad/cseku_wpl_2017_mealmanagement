Meal Management
------------------
This is a subsection of 'University Automation System'. It is created with the help of 'SimTier' framework. Under the 'Web Programming Laboratory' course of Computer Science and Engineering Discipline of Khulna University,Khulna,Bangladesh. 

Most of the meal related works can be done with the help of it. 

Admin can CRUD food,meal,menu and also can offer menu to be ordered. can also see all the orders that have been made.
User can see offered menu list and can order from there and can check all the orders that have been made by him and the total cost of that.


--------------------
Developed By
-------------
S.M. AZOAD AHNAF
& 
SUDIPTO DAS
Student
Computer Science and Engineering Discipline
Khulna University
Bangladesh

------------------------
Author and Supervisor
----------------
Dr. KAZI MASUDUL ALAM
Assistant Professor
CSE Discipline, Khulna University
Bangladesh




---------------------------------------------------------------------------------------------------------
SimTier v0.2 
---------------
This is a simple 3-Tier framework for the Php web applications. The framework follows Object Oriented Design principle for project development. The framework comes with different utilities to handle database (MySQL) and XML operations. It is a modular system, where new templates for visualization can be incorporated.  

This is a beta version of the framework that is still in the early stage of development. New features and capabilities will be incorporated to the framework time to time.

Version: 0.2 (beta)
----------------------

Author
------------
Dr. KAZI MASUDUL ALAM
Assistant Professor
CSE Discipline, Khulna University
Bangladesh

